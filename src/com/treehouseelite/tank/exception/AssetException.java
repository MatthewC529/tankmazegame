package com.treehouseelite.tank.exception;

/**
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 * @Lib LightWeight Java Gaming Library and Slick2D Library
 * 
 * @Thanks Michael Lucido (Best Friend, Alpha Tester, Treehouse Elite)
 * @Thanks Kevin MacLeod (Royalty Free Music)
 * 
 * @License GNU LGPL v3 in Concurrence with GNU GPL v3
 * @Copyright GNU LGPL v3 in Concurrence with GNU GPL v3
 * 
 * @Warranty This file is part of Foobar.
 * 
 *           Tank Maze Game is free software: you can redistribute it and/or
 *           modify it under the terms of the GNU Lesser General Public License
 *           in concurrence with the GNU General Public License as published by
 *           the Free Software Foundation, either version 3 of the License, or
 *           any later version.
 * 
 *           Tank Maze Game is distributed in the hope that it will be
 *           entertaining, but WITHOUT ANY WARRANTY; without even the implied
 *           warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *           See the GNU Lesser General Public License and GNU General Public
 *           License for more details.
 * 
 *           You should have received a copy of the GNU General Public License
 *           and GNU Lesser General Public License along with Tank Maze Game. If
 *           not, see <http://www.gnu.org/licenses/>
 **/

public class AssetException extends Exception {
	
	// What is the name of the boolean?, Result?, Or is it not a boolean...,What
	// Class?
	public AssetException(String booleanName, boolean result, boolean nonBoolean, Class classOccur, String location) {
		if (!nonBoolean) {
			if (booleanName.equalsIgnoreCase("completeSpriteSheets")) {
				if (!result) {
					SpriteException(location, classOccur);
				} else if (result) {
					System.out.println("There was no exception, but one was thrown at " + location);
				}
			}
		} else {
			nonBooleanCheck(location, classOccur);
		}
	}
	
	protected void SpriteException(String location, Class exceptClass) {
		System.out.println(String.format("%s contains a SpriteSheet Exception!\n\t at %s in initAssets", location, exceptClass.getPackage()));
		
	}
	
	protected void nonBooleanCheck(String location, Class whatClass) {
		
	}
	
}
