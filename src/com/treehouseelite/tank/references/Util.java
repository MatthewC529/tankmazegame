package com.treehouseelite.tank.references;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.newdawn.slick.Color;

import com.treehouseelite.tank.TankMaze;
import com.treehouseelite.tank.exception.LogException;

/**
 * Houses many of the utilities used in the game such as score conversion, date
 * gathering, saving, color conversion, URL conversion, etc.
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * @category Utilities Class
 */
public class Util {
	
	/**
	 * Receives the current date and time in the format MM/dd/yyyy HH:mm
	 * 
	 * @return Current Date and Time
	 * @author Matthew Crocco (Treehouse Elite)
	 */
	public static String getTime() {
		String t = "NULL";
		
		DateFormat dForm = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Date date = new Date();
		
		t = dForm.format(date);
		
		return t;
	}
	
	/**
	 * Formats the score by passing an int value and receiving the Score in the
	 * form of a string, is filled with 0's to maintain a constant string
	 * size... I have never tried breaking the limit
	 * 
	 * @param score
	 *            - Int value of the current player score
	 * @return Score as a String data type
	 * 
	 * @author Matthew Crocco (Treehouse Elite)
	 */
	public static String convertScore(int score) {
		String s = "NULL";
		
		String curScore = Integer.toString(score);
		while (curScore.length() < 9) {
			curScore = String.format("%s%s", "0", curScore);
		}
		s = curScore;
		return s;
	}
	
	/**
	 * Gets the length of the file passed to it
	 * 
	 * @param filename
	 *            - Path of the file to be measured
	 * @return Length of file in the form on an int value
	 * @throws IOException
	 * 
	 * @author Matthew Crocco (Treehouse Elite)
	 */
	public static int getFileLength(String filename) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(filename));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return ((count == 0) && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}
	
	/**
	 * Converts URL to a String for paths and the such
	 * 
	 * @param url
	 *            - URL to be converted
	 * @return URL in the form of a nice, clean string
	 * @author Matthew Crocco (Treehouse Elite)
	 */
	public static String convertURL(URL url) {
		String convert = null;
		StringBuilder sb = new StringBuilder();
		boolean initialDelete = true;
		boolean deleteMode = false;
		boolean oneTimeDeleteMode = false;
		int initDeleteCount = 0;
		
		char[] charArr = url.toString().toCharArray();
		boolean deleteOnce = false;
		
		for (int i = 0; i < charArr.length; i++) {
			if (!initialDelete) {
				char target = charArr[i];
				if ((target == '2') || deleteOnce) {
					if (charArr[i + 1] == '0') {
						sb.append("");
						deleteOnce = true;
					} else if (deleteOnce) {
						sb.append("");
						deleteOnce = false;
					}
				}
				if ((target == '%') || deleteMode) {
					deleteMode = true;
					sb.append("");
					initDeleteCount++;
					if (deleteMode && (initDeleteCount == 4)) {
						deleteMode = false;
						initDeleteCount = 0;
						sb.append(" ");
						sb.append(target);
						
					}
				} else if ((target == '/') && (charArr[i + 1] == '.') && !oneTimeDeleteMode) {
					if ((charArr[i + 1] == '.') && !oneTimeDeleteMode) {
						oneTimeDeleteMode = true;
						sb.append(target);
					}
				} else if (oneTimeDeleteMode) {
					if (oneTimeDeleteMode && (initDeleteCount == 2)) {
						oneTimeDeleteMode = false;
						sb.append(target);
					} else if (oneTimeDeleteMode && (initDeleteCount != 2)) {
						initDeleteCount++;
					}
				}
				
				else {
					sb.append(target);
				}
				
			} else {
				if (initDeleteCount == 6) {
					initialDelete = false;
					sb.append(charArr[i]);
					initDeleteCount = 0;
				} else {
					initDeleteCount++;
					sb.append("");
				}
				
			}
			
		}
		
		convert = sb.toString();
		if (!convert.substring(convert.lastIndexOf('2'), convert.lastIndexOf('2') + 2).equals("Should not equal")) {
			System.out.println("Does Contain!");
			CharSequence cs = "20";
			System.out.println(convert.substring(convert.lastIndexOf('2'), convert.lastIndexOf('2') + 2));
			convert.substring(convert.lastIndexOf('2'), convert.lastIndexOf('2') + 2).replace(cs, "");
		}
		System.out.println(convert);
		System.out.println(url.toString());
		return convert;
	}
	
	// 0%,25%,50%,75%,100% Alpha Values for colors
	/** Color will Have 100% Opacity or 0% Transparency (Solid) */
	public static final float ALPHA_FULL = 1f;
	/** Color will Have 75% Opacity or 25% Transparency */
	public static final float ALPHA_TOPQUARTER = 0.75f;
	/** Color will Have 50% Opacity or 50% Transparency (Faded) */
	public static final float ALPHA_HALF = 0.5f;
	/** Color will Have 25% Opacity or 75% Transparency */
	public static final float ALPHA_BOTTOMQUARTER = 0.25f;
	/** Color will Have 0% Opacity or 100% Transparency (Invisible) */
	public static final float ALPHA_TRANSPARENT = 0.0f;
	
	/**
	 * Creates a Color from a hex value
	 * 
	 * @param hex
	 *            - Hex Value to create Color from in the form of a String
	 * @return Color created from Hex
	 * @author Matthew Crocco (Treehouse Elite)
	 */
	public static Color convertHex(String hex) {
		
		int red = Integer.valueOf(hex.substring(0, 2), 16);
		int green = Integer.valueOf(hex.substring(2, 4), 16);
		int blue = Integer.valueOf(hex.substring(4, 6), 16);
		
		if ((red > 1) || (green > 1) || (blue > 1)) {
			red /= 255.0f;
			green /= 255.0f;
			blue /= 255.0f;
		}
		
		return new Color(red, green, blue);
	}
	
	/**
	 * !!WARNING - Returns a Slick2D Color Object!, Use convertNormHex To Get a
	 * AWT Color!! ------------------------------------ Creates a Color from a
	 * hex value, allows a varied transparency
	 * 
	 * @param hex
	 *            - Hex Value to create a Color from
	 * @param alpha
	 *            - Opacity
	 * @return Color created from Hex and Alpha values - SLICK2D Color!
	 * @see Util.convertNormHex
	 * @see org.newdawn.slick.Color
	 */
	public static Color convertHex(String hex, float alpha) {
		
		float red = Integer.valueOf(hex.substring(0, 2), 16);
		float green = Integer.valueOf(hex.substring(2, 4), 16);
		float blue = Integer.valueOf(hex.substring(4, 6), 16);
		float a = alpha;
		
		// May need to be changed, What if Red > 1 but Green = 0.1? Then you get
		// a different color
		if ((red > 1) || (green > 1) || (blue > 1)) {
			red /= 255.0f;
			green /= 255.0f;
			blue /= 255.0f;
			
		}
		
		if (a > 1) {
			a /= 100;
		}
		
		return new Color(red, green, blue, a);
	}
	
	/**
	 * !!WARNNG -- Creates a AWT Color Object! For a Slick2D Color Object use
	 * convertHex!! -------------------------------------------Creates a Color
	 * Object from a Hex Value in the form of a String
	 * 
	 * @param hex
	 *            -- Hex Value in the Form of a String
	 * @param alpha
	 *            -- Opacity Value
	 * @return java.awt.Color
	 * @author Matthew Crocco (Treehouse Elite)
	 * @see Util.convertHex
	 * @see java.awt.Color
	 */
	public static java.awt.Color convertNormHex(String hex, float alpha) {
		
		float red = Integer.valueOf(hex.substring(0, 2), 16);
		float green = Integer.valueOf(hex.substring(2, 4), 16);
		float blue = Integer.valueOf(hex.substring(4, 6), 16);
		float a = alpha;
		
		// May need to be changed, What if Red > 1 but Green = 0.1? Then you get
		// a different color
		if ((red > 1) || (green > 1) || (blue > 1)) {
			red /= 255.0f;
			green /= 255.0f;
			blue /= 255.0f;
			
		}
		
		if (a > 1) {
			a /= 100;
		}
		
		return new java.awt.Color(red, green, blue, a);
	}
	
	static Log log;
	
	/**
	 * Finalizes the Log object and runs System Finalizers to allow closing
	 * 
	 * @author Matthew Crocco (Treehouse Elite)
	 * @throws LogException
	 */
	public static void conductFinalization() throws LogException {
		log = TankMaze.log;
		org.lwjgl.openal.AL.destroy();
		log.finishUp();
		System.runFinalization();
	}
	
}
