package com.treehouseelite.tank.game.handlers;

/** 
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 * @Lib LightWeight Java Gaming Library and Slick2D Library
 * 
 * @Thanks Michael Lucido (Best Friend, Alpha Tester, Treehouse Elite)
 * @Thanks Kevin MacLeod (Royalty Free Music)
 * 
 * @License GNU LGPL v3 in Concurrence with GNU GPL v3
 * @Copyright GNU LGPL v3 in Concurrence with GNU GPL v3 
 * 
 * @Warranty 
 *     This file is part of Foobar.
 *
 *  Tank Maze Game is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License in concurrence with the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  Tank Maze Game is distributed in the hope that it will be entertaining,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License and GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License and
 *  GNU Lesser General Public License along with Tank Maze Game.  If not, see <http://www.gnu.org/licenses/>
 **/

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.tiled.TiledMap;

import com.treehouseelite.tank.references.Reference;

//import java.awt.Rectangle;

public class TileHandler {
	
	public static String mapPath = "res/World/level_";
	
	public static int bg, paths, collision;
	
	public static Image[][] tMapTiles = new Image[25][20];
	public static Animation[][] tMapAnimated = new Animation[25][20];
	
	public static boolean[][] collidableTile = new boolean[25][20];
	
	static Graphics g = new Graphics();
	static AssetHandler asset = new AssetHandler();
	
	// The Amount of Image is too damn high!
	
	static TiledMap tMap;
	
	public static int wFrame = 0;
	
	private static int id;
	
	public Rectangle[][] tileLayer = new Rectangle[25][20];
	public static ArrayList<Rectangle> collisionTiles = new ArrayList<Rectangle>(500);
	static Rectangle r;
	
	public TileHandler() {
		
	}
	
	public Rectangle[][] createTiles(int id, Rectangle layer) throws SlickException {
		// Scans 0,0 to 0,20 of the tiles and then moves down the x line
		// gettings tiles
		// 0,0 = tileLayer[0][0]
		Rectangle[][] layerKinda = null;
		TileHandler.id = id;
		mapPath = String.format("res/World/level_%s.tmx", id);
		
		tMap = new TiledMap(mapPath);
		
		bg = tMap.getLayerIndex("background");
		paths = tMap.getLayerIndex("paths");
		collision = tMap.getLayerIndex("collision");
		// Constructs a Grid of Rectangles based on the Map's Top Left point
		
		for (int i = 0; i < tileLayer.length; i++) {
			for (int y = 0; y < tileLayer[0].length; y++) {
				Rectangle tile = new Rectangle((i + Reference.MAP_X) + (i * Reference.TILE_SIZE), (y + Reference.MAP_Y) + (y * Reference.TILE_SIZE), 32, 32);
				tileLayer[i][y] = tile;
			}
		}
		
		Reference.tiledLayer = tileLayer;
		layerKinda = tileLayer;
		return layerKinda;
		/*
		 * for(int x = 0; x<collisionTiles.length; x++){ for(int y = 0;
		 * y<collisionTiles[0].length; y++){ Rectangle tile = new
		 * Rectangle((x+Reference.MAP_X) + (x*31),
		 * (y+Reference.MAP_Y+14)+(y*31),32,32); collisionTiles[x][y] = tile; }
		 * }
		 */
		
	}
	
	/** @deprecated */
	@Deprecated
	public static void initSprites(Rectangle[][] layer) {
		
		bg = tMap.getLayerIndex("background");
		paths = tMap.getLayerIndex("paths");
		collision = tMap.getLayerIndex("collision");
		
		System.out.println("Initialized Sprites!");
		
	}
	
	// Initializes all tiles and put them into Image and Boolean Arrays
	// Boolean Array for later use with determining whether the player or entity
	// can be there. (collidableTile)
	// Image array holds the tiles (tMapTiles)
	public static void initTileMap() throws SlickException {
		new Graphics();
		
		// Getting Tiles based off Tile ID's
		/** DIRT PATH MAPS (Dev Map, Level 1) **/
		
		if ((id == 0) || (id == 1)) {
			for (int x = 0; x < Reference.tiledLayer.length; x++) {
				for (int y = 0; y < Reference.tiledLayer[0].length; y++) {
					
					float cornerX = Reference.tiledLayer[x][y].getX();
					float cornerY = Reference.tiledLayer[x][y].getY();
					Rectangle r = new Rectangle(cornerX, cornerY, 32, 32);
					
					if (tMap.getTileId(x, y, bg) == 1) {
						tMapTiles[x][y] = AssetHandler.sparseGrass;
						
					}
					
					if (tMap.getTileId(x, y, collision) == 2) {
						tMapAnimated[x][y] = AssetHandler.vRiver1;
						
						collisionTiles.add(r);
						
					}
					if (tMap.getTileId(x, y, collision) == 57) {
						tMapTiles[x][y] = AssetHandler.concrete1;
						
						collisionTiles.add(r);
						
					}
					if (tMap.getTileId(x, y, collision) == 71) {
						tMapTiles[x][y] = AssetHandler.concrete2;
						
						// collisionTiles.add(new
						// Rectangle(tileLayer[x][y].getX(),
						// tileLayer[x][y].getY()+14, 32, 32)) ;
						collisionTiles.add(r);
					}
					if (tMap.getTileId(x, y, collision) == 85) {
						tMapTiles[x][y] = AssetHandler.concrete3;
						
						collisionTiles.add(r);
						
					}
					if (tMap.getTileId(x, y, collision) == 72) {
						tMapTiles[x][y] = AssetHandler.metal1;
						
						collisionTiles.add(r);
						
					}
					if (tMap.getTileId(x, y, collision) == 58) {
						tMapTiles[x][y] = AssetHandler.metal2;
						
						collisionTiles.add(r);
						
					}
					if (tMap.getTileId(x, y, paths) == 50) {
						tMapTiles[x][y] = AssetHandler.hDirtPath;
						
					}
					if (tMap.getTileId(x, y, paths) == 60) {
						tMapTiles[x][y] = AssetHandler.dirtPath;
						
					}
					if (tMap.getTileId(x, y, paths) == 59) {
						tMapTiles[x][y] = AssetHandler.dirtPathTurn4;
						
					}
					if (tMap.getTileId(x, y, paths) == 73) {
						tMapTiles[x][y] = AssetHandler.dirtPathTurn3;
						
					}
					if (tMap.getTileId(x, y, paths) == 79) {
						tMapTiles[x][y] = AssetHandler.dirtThreewayRight;
						
					}
					if (tMap.getTileId(x, y, paths) == 46) {
						tMapTiles[x][y] = AssetHandler.dirtPathTurn1;
						
					}
					if (tMap.getTileId(x, y, paths) == 37) {
						tMapAnimated[x][y] = AssetHandler.hDirtCrossing;
						
					}
					if ((tMap.getTileId(x, y, paths) == 80) || (tMap.getTileId(x, y, paths) == 88)) {
						tMapTiles[x][y] = AssetHandler.dirtThreewayLeft;
						
					}
					if (tMap.getTileId(x, y, paths) == 102) {
						tMapTiles[x][y] = AssetHandler.dirtThreewayDown;
						
					}
					if (tMap.getTileId(x, y, paths) == 74) {
						tMapTiles[x][y] = AssetHandler.dirtPathTurn2;
						
					}
					if (tMap.getTileId(x, y, paths) == 107) {
						tMapTiles[x][y] = AssetHandler.dirtThreewayUp;
						
					}
					if (tMap.getTileId(x, y, paths) == 88) {
						tMapTiles[x][y] = AssetHandler.dirtCrossroads;
						
					}
					
				}
			}
		}
		
	}
	
	public void drawTileMap(Rectangle[][] layer, Image[][] tiles, Graphics g) {
		
		// Loops through the Image array and places tile based on the Top Left
		// corner of the Rectangle in the rectangle array
		// Rectangle Array = layer (tileLayer was passed)
		// Image Array = tiles (tMapTiles was passed)
		// Asset Refers to Asset Handler
		for (int x = 0; x < layer.length; x++) {
			for (int y = 0; y < layer[0].length; y++) {
				if (tiles[x][y] != null) {
					g.drawImage(tiles[x][y], layer[x][y].getX(), layer[x][y].getY());
				}
				if (tMapAnimated[x][y] != null) {
					g.drawAnimation(tMapAnimated[x][y], layer[x][y].getX(), layer[x][y].getY());
				} else {
					;
				}
			}
		}
	}
	
}
/*
 * FOR renderTileMap() -- Left Over Artefact/Dead Code in case of use.. CAUSES
 * MASSIVE FPS DROP... dropped for Use of Tiled Methods
 * 
 * int iteration = 0; int counter = 0;
 * 
 * for(int x = 0 ; x<tileLayer.length;x++){ for(int y = 0;
 * y<tileLayer[0].length; y++){ int selection = 1; if (iteration == 0 ||
 * iteration == 1 || iteration == 3){
 * 
 * if(!grassRenderDone){ Random r = new Random(); tileLocs[x][y] =
 * r.nextInt(3)+1;
 * 
 * selection = tileLocs[x][y]; }else{
 * 
 * selection = tileLocs[x][y]; if(selection == 0){ selection = 1;
 * System.out.println
 * ("Error in Saving Tile Locations! -- Rendering sparseGrass As Default!+"); }
 * }
 * 
 * 
 * if(selection == 1){ g.drawImage(sparseGrass, tileLayer[x][y].getX(),
 * tileLayer[x][y].getY());
 * 
 * }else if(selection == 2){ g.drawImage(denseGrass, tileLayer[x][y].getX(),
 * tileLayer[x][y].getY());
 * 
 * }else if(selection == 3){ g.drawImage(denserGrass, tileLayer[x][y].getX(),
 * tileLayer[x][y].getY());
 * 
 * } if(!firstRender){
 * 
 * firstRender = true; } collide.renderObstacles(tileLayer, g);
 * collide.renderPaths(tileLayer, g); }
 * 
 * } }
 * 
 * grassRenderDone = true; iteration++;
 */