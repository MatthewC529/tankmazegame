/**
 * 
 */
package com.treehouseelite.tank.game.handlers;

import org.newdawn.slick.Input;

import com.treehouseelite.tank.references.Reference;
import com.treehouseelite.tank.testbed.TEConsole;

/**
 * @author Matthew Crocco (Treehouse Elite)
 * 
 */
@SuppressWarnings("unused")
public class InputHandler {
	
	public static final String KEYBOARD = "Keyboard";
	public static final String MOUSE = "Mouse";
	
	private static final int PLAY_BUTTON = 0;
	private static final int SETTINGS_BUTTON = 1;
	private static final int EXTRAS_BUTTON = 2;
	private static final int EXIT_BUTTON = 3;
	
	private long timer;
	private long wait;
	
	private static boolean activatedConsole = false;
	private static boolean consoleUp = false;
	
	private Input in;
	private String type;
	
	private static TEConsole console;
	
	public InputHandler(Input in, String type) {
		this.type = type;
		this.in = in;
		
		if (this.type.equalsIgnoreCase(KEYBOARD)) {
			keysUpdate();
		}
		if (this.type.equalsIgnoreCase(MOUSE)) {
			mouseUpdate();
		}
		
		if (activatedConsole) {
			updateConsole();
		}
	}
	
	private void keysUpdate() {
		if (in.isKeyPressed(Input.KEY_C) && Reference.debug) {
			if (activatedConsole) {
				activatedConsole = false;
				consoleUp = false;
				console.dispose();
				timer = System.currentTimeMillis();
				wait = System.currentTimeMillis() + (5 * 1000);
			}
			if (!activatedConsole && (timer >= wait)) {
				activatedConsole = true;
			} else if (timer != wait) {
				timer = System.currentTimeMillis();
			}
		}
		
	}
	
	private void mouseUpdate() {
		
	}
	
	public void updateConsole() {
		if (!consoleUp) {
			console = new TEConsole();
			consoleUp = true;
		}
	}
	
}
