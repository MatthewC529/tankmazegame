package com.treehouseelite.tank.game.handlers;

import java.util.ArrayList;

import org.newdawn.slick.geom.Point;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Collision {
	
	ArrayList<Rectangle> collisionRects = TileHandler.collisionTiles;
	
	public boolean curCollide;
	public String type;
	
	public Collision(Shape s) {
		
		type = "Single Point Entity";
		curCollide = isCollide(s);
		
	}
	
	public Collision(Point[] cPoints, String str) {
		
	}
	
	public boolean isCollide(Shape s) {
		
		Rectangle currentRect;
		boolean currentBool;
		
		currentRect = getTile(s);
		currentBool = getTile(currentRect);
		
		return currentBool;
		
	}
	
	protected Rectangle getTile(Shape s) {
		
		Rectangle r = null;
		
		for (int x = 0; x < collisionRects.size(); x++) {
			if (s.intersects(collisionRects.get(x))) {
				r = collisionRects.get(x);
				OutputHandler.write(String.format("cRect X: %s, Y: %s", collisionRects.get(x).getX(), collisionRects.get(x).getY()), 200, 150);
				break;
			}
		}
		if (r == null)
			return r;
		else
			return r;
	}
	
	protected boolean getTile(Rectangle r) {
		
		if (r == null)
			return false;
		else
			return true;
	}
	
}
