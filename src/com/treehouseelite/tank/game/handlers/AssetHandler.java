package com.treehouseelite.tank.game.handlers;

/** 
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 * @Lib LightWeight Java Gaming Library and Slick2D Library
 * 
 * @Thanks Michael Lucido (Best Friend, Alpha Tester, Treehouse Elite)
 * @Thanks Kevin MacLeod (Royalty Free Music)
 * 
 * @License GNU LGPL v3 in Concurrence with GNU GPL v3
 * @Copyright GNU LGPL v3 in Concurrence with GNU GPL v3 
 * 
 * @Warranty 
 *     This file is part of Foobar.
 *
 *  Tank Maze Game is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License in concurrence with the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  Tank Maze Game is distributed in the hope that it will be entertaining,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License and GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License and
 *  GNU Lesser General Public License along with Tank Maze Game.  If not, see <http://www.gnu.org/licenses/>
 **/

import java.io.IOException;
import java.net.URL;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import com.treehouseelite.tank.exception.AssetException;
import com.treehouseelite.tank.exception.LogException;
import com.treehouseelite.tank.references.Log;

public class AssetHandler {
	
	public static boolean isComplete = false;
	
	private static String musPath = "res/Sounds/";
	static TileHandler tile;
	static Log log;
	TextureLoader tex = new TextureLoader();
	
	private static int tsize = 32;
	
	// TODO convert all resources to use the Slick ResourceLoader... lets just
	// go with it.
	
	private static URL alphabetPath = ResourceLoader.getResource("res/GameSprites/Maze Game/alphabet.png");
	private static URL spritesPath = ResourceLoader.getResource("res/GameSprites/Maze Game/sprites.png");
	private static URL terrainPath = ResourceLoader.getResource("res/GameSprites/Maze Game/terrain.png");
	private static URL twPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/tankToWest.png");
	private static URL tePath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/tankToEast.png");
	private static URL tnPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/tankToNorth.png");
	private static URL tsPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/tankToSouth.png");
	private static URL bwPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/blueToWest.png");
	private static URL bePath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/blueToEast.png");
	private static URL bnPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/blueToNorth.png");
	private static URL bsPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/blueToSouth.png");
	
	private static URL menuButtonPath0 = ResourceLoader.getResource("res/GameSprites/Maze Game/menu/MainMenuButtonnohover.png");
	private static URL menuButtonPath1 = ResourceLoader.getResource("res/GameSprites/Maze Game/menu/MainMenuButtonhover.png");
	private static URL errorImagePath = ResourceLoader.getResource("res/GameSprites/Maze Game/menu/ImageError.png");
	
	private static URL rePath, rwPath, rsPath, rnPath;
	private static URL pePath, pwPath, psPath, pnPath;
	private static URL oePath, owPath, osPath, onPath;
	
	public static SpriteSheet sprites, alphabet;
	public static SpriteSheet terrain, tankWest, tankEast, tankNorth, tankSouth, blueN, blueS, blueE, blueW, pinkN, pinkS, pinkW, pinkE, redN, redS, redE, redW, orangeN, orangeS, orangeW, orangeE;
	private static SpriteSheet ErrorImage, MenuButtonImageOff, MenuButtonImageOn;
	
	public static Animation tankToWest;
	public static Animation tankToEast;
	public static Animation tankToNorth;
	public static Animation tankToSouth, vRiver1, vRiver2, vRiver3, vDirtCrossing, vStoneBridge, vStreetBridge, hRiver1, hRiver2, hRiver3, hDirtCrossing, hStoneBridge, hStreetBridge;
	public static Animation blueToNorth, blueToSouth, blueToEast, blueToWest, redToNorth, redToSouth, redToEast, redToWest, pinkToNorth, pinkToSouth, pinkToWest, pinkToEast, orangeToNorth, orangeToSouth, orangeToEast, orangeToWest;
	
	// Animation Images
	public static Image sparseGrass, denseGrass, denserGrass, water11, water12, water13, water21, water22, water23, water31, water32, water33, water41, water42, water43, water51, water52, water53, water61, water62, water63, concrete1, concrete2, concrete3, metal1, metal2, dirtPathTurn1,
			dirtPathTurn2, dirtPath, hDirtPath, streetTurn1, streetTurn2, streetPath, hStreetPath, stoneTurn1, stoneTurn2, stonePath, hStonePath, dirtCrossroads, streetCrossroads, stoneCrossroads, dirtThreewayDown, streetThreewayDown, stoneThreewayDown, hDirtCrossing1, hDirtCrossing2,
			hDirtCrossing3, hStoneBridge1, hStoneBridge2, hStoneBridge3, hStreetBridge1, hStreetBridge2, hStreetBridge3, vDirtCrossing1, vDirtCrossing2, vDirtCrossing3, vStoneBridge1, vStoneBridge2, vStoneBridge3, vStreetBridge1, vStreetBridge2, vStreetBridge3, dirtPathTurn3, dirtPathTurn4,
			streetTurn3, streetTurn4, stoneTurn3, stoneTurn4, dirtThreewayUp, stoneThreewayUp, streetThreewayUp, dirtThreewayRight, dirtThreewayLeft, stoneThreewayRight, stoneThreewayLeft, streetThreewayRight, streetThreewayLeft, tankNorth1, tankNorth2, tankNorth3, tankNorth4, tankNorth5,
			tankNorth6, tankNorth7, tankSouth1, tankSouth2, tankSouth3, tankSouth4, tankSouth5, tankSouth6, tankSouth7;
	
	public static Image menuButton0, menuButton1, errorImage;
	
	public static Music titleMus1, titleMus2, titleMus3, loadingScreenMus1, loadingScreenMus2, loadingScreenMus3;
	
	public static Sound tankMove, tankFire, tankExplode, tankSurrender, tankRetreat;
	
	public void initAssets() throws AssetException {
		try {
			log = new Log();
		} catch (LogException e) {
			e.printStackTrace();
		}
		System.out.println("========================INITIALIZING ALL ASSETS=======================");
		System.out.println("Initializing SpriteSheets...");
		
		try {
			log.writeToFile("-----------------------INITIALIZE SPRITESHEETS--------------------------", Log.INFO);
			rePath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/redToEast.png");
			rwPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/redToWest.png");
			rnPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/redToNorth.png");
			rsPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/redToSouth.png");
			
			pePath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/pinkToEast.png");
			pwPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/pinkToWest.png");
			pnPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/pinkToNorth.png");
			psPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/pinkToSouth.png");
			
			oePath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/orangeToEast.png");
			owPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/orangeToWest.png");
			onPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/orangeToNorth.png");
			osPath = ResourceLoader.getResource("res/GameSprites/Maze Game/animation/orangeToSouth.png");
			
			alphabet = new SpriteSheet(alphabetPath, tsize / 2, tsize / 2);
			sprites = new SpriteSheet(spritesPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", spritesPath));
			log.writeToFile(String.format("%s : Asset Initialized!...", spritesPath.toString()), Log.DEBUG);
			terrain = new SpriteSheet(terrainPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", terrainPath));
			log.writeToFile(String.format("%s : Asset Initialized!...", terrainPath.toString()), Log.DEBUG);
			tankWest = new SpriteSheet(twPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", twPath));
			log.writeToFile(twPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			tankEast = new SpriteSheet(tePath, tsize, tsize);
			log.writeToFile(tePath.toString() + " : Asset Initialized!...", Log.DEBUG);
			System.out.println(String.format("%s : Asset Initialized!...", tePath));
			tankNorth = new SpriteSheet(tnPath, tsize, tsize);
			log.writeToFile(tnPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			System.out.println(String.format("%s : Asset Initialized!...", tnPath));
			tankSouth = new SpriteSheet(tsPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", tsPath));
			log.writeToFile(tsPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			blueN = new SpriteSheet(bnPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", bnPath));
			log.writeToFile(bnPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			blueS = new SpriteSheet(bsPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", bsPath));
			log.writeToFile(bsPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			blueE = new SpriteSheet(bePath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", bePath));
			log.writeToFile(bePath.toString() + " : Asset Initialized!...", Log.DEBUG);
			blueW = new SpriteSheet(bwPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", bwPath));
			log.writeToFile(bwPath.toString() + " : Asset Initialized!", Log.DEBUG);
			redN = new SpriteSheet(rnPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", rnPath));
			log.writeToFile(rnPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			redS = new SpriteSheet(rsPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", rsPath));
			log.writeToFile(rsPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			redE = new SpriteSheet(rePath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", rePath));
			log.writeToFile(rePath.toString() + " : Asset Initialized!...", Log.DEBUG);
			redW = new SpriteSheet(rwPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", rwPath));
			log.writeToFile(rwPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			pinkN = new SpriteSheet(pnPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", pnPath));
			log.writeToFile(pnPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			pinkS = new SpriteSheet(psPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", psPath));
			log.writeToFile(psPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			pinkE = new SpriteSheet(pePath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", pePath));
			log.writeToFile(pePath.toString() + " : Asset Initialized!...", Log.DEBUG);
			pinkW = new SpriteSheet(pwPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", pwPath));
			log.writeToFile(pwPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			orangeN = new SpriteSheet(onPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", onPath));
			log.writeToFile(onPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			orangeS = new SpriteSheet(osPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", osPath));
			log.writeToFile(osPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			orangeE = new SpriteSheet(oePath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", oePath));
			log.writeToFile(oePath.toString() + " : Asset Initialized!...", Log.DEBUG);
			orangeW = new SpriteSheet(owPath, tsize, tsize);
			System.out.println(String.format("%s : Asset Initialized!...", owPath));
			log.writeToFile(owPath.toString() + " : Asset Initialized!...", Log.DEBUG);
			System.out.println("SpriteSheets Initialized...");
			log.writeToFile("-----------------------FINALIZED SPRITESHEETS--------------------------", Log.INFO);
			log.writeToFile("Man that was a mess to Refactor....", Log.SUPERFLUOUS);
			log.writeToFile("\"My backstory is so tedious.\" - Ray LaMontagne", Log.SUPERFLUOUS);
			log.writeToFile("-----------------------------------------------------------------------", Log.INFO);
			
			ErrorImage = new SpriteSheet(errorImagePath, 100, 100);
			MenuButtonImageOff = new SpriteSheet(menuButtonPath0, 326, 100);
			MenuButtonImageOn = new SpriteSheet(menuButtonPath1, 326, 100);
			
			errorImage = ErrorImage.getSprite(0, 0);
			menuButton0 = MenuButtonImageOff.getSprite(0, 0);
			menuButton1 = MenuButtonImageOn.getSprite(0, 0);
			
			log.writeToFile("Quickly Initialized Menu Images.... Aren't I A Tricky Bastard? ;)", Log.INFO);
			// I finally decided here to make a Log Method! So I dont have to
			// type out log.writeToFile.... damnit....+
			initSprites();
		} catch (SlickException | IOException e) {
			e.printStackTrace();
			try {
				log();
				log(String.format("%s\nInitialization Error(s) at %s", e.getMessage(), this.getClass().getPackage().getName() + " -------- " + this.getClass().getName()), Log.ERROR);
			} catch (IOException e1) {
			}
			
			System.out.println("FAILURE TO INITIALIZE SPRITESHEETS at com.treehouseelite.tank.game.handlers.AssetHandler.initAssets: Line 18");
		}
		
	}
	
	protected void initSprites() {
		boolean completeAnimations = false;
		
		System.out.println("---------------INITIALIZING ANIMATIONS AND SPRITES---------------");
		try {
			log.writeToFile("-----------------------INITIALIZE ANIMATIONS AND SPRITES--------------------------", Log.INFO);
			SpriteSheet terrain = AssetHandler.terrain;
			
			/** INITIALIZATION OF BACKGROUND AND OBSTACLES **/
			sparseGrass = terrain.getSprite(0, 0);
			denseGrass = terrain.getSprite(0, 1);
			denserGrass = terrain.getSprite(0, 2);
			
			// This format used for ALL animations
			
			/** ALL ANIMATIONS AND THEIR RESPECTIVE TILES/FRAMES **/
			
			/* VERTICAL RIVER ANIMATION AND TILES */
			// This relates to the Top River animation in the spritesheet!
			water11 = terrain.getSprite(1, 0); // 1(Top):1(First Frame of
												// Animation)
			water12 = terrain.getSprite(2, 0); // 1(Top):2(Second Frame of
												// Animation)
			water13 = terrain.getSprite(3, 0); // 1(Top):3(Third Frame of
												// Animation)
			
			// Relates to Mid River animation
			water21 = terrain.getSprite(1, 1); // 2(Mid):1(First Frame of
												// Animation)
			water22 = terrain.getSprite(2, 1); // 2(Mid):2(Second Frame of
												// Animation)
			water23 = terrain.getSprite(3, 1); // 2(Mid):3(Third Frame of
												// Animation)
			
			// Relates to Bottom River Animation
			water31 = terrain.getSprite(1, 2); // 3(Bottom):1(First Frame of
												// Animation)
			water32 = terrain.getSprite(2, 2); // 3(Bottom):2(Second Frame of
												// Animation)
			water33 = terrain.getSprite(3, 2); // 3(Bottom):3(Third Frame of
												// Animation)
			
			/* HORIZONTAL RIVER ANIMATION AND TILES */
			
			// Relates to Far Left of River
			water41 = terrain.getSprite(4, 0);
			water42 = terrain.getSprite(5, 0);
			water43 = terrain.getSprite(6, 0);
			
			// relates to Middle of River Horizontally
			water51 = terrain.getSprite(4, 1);
			water52 = terrain.getSprite(5, 1);
			water53 = terrain.getSprite(6, 1);
			
			// relates to Far Right of River
			water61 = terrain.getSprite(4, 2);
			water62 = terrain.getSprite(5, 2);
			water63 = terrain.getSprite(6, 2);
			
			System.out.println("Initialized All Water Sprites...");
			log.writeToFile("Water Animations Initialized!...", Log.DEBUG);
			
			tankNorth1 = sprites.getSprite(0, 4);
			tankNorth2 = sprites.getSprite(1, 4);
			tankNorth3 = sprites.getSprite(2, 4);
			tankNorth4 = sprites.getSprite(3, 4);
			tankNorth5 = sprites.getSprite(4, 4);
			tankNorth6 = sprites.getSprite(5, 4);
			tankNorth7 = sprites.getSprite(6, 4);
			
			tankSouth1 = sprites.getSprite(0, 3);
			tankSouth2 = sprites.getSprite(1, 3);
			tankSouth3 = sprites.getSprite(2, 3);
			tankSouth4 = sprites.getSprite(3, 3);
			tankSouth5 = sprites.getSprite(4, 3);
			tankSouth6 = sprites.getSprite(5, 3);
			tankSouth7 = sprites.getSprite(6, 3);
			
			System.out.println("Initialized All Entity Animations...");
			
			/* HORIZONTAL RIVER CROSSING ANIMATION AND TILES */
			
			hDirtCrossing1 = terrain.getSprite(8, 2);
			hDirtCrossing2 = terrain.getSprite(9, 2);
			hDirtCrossing3 = terrain.getSprite(10, 2);
			
			hStoneBridge1 = terrain.getSprite(8, 1);
			hStoneBridge2 = terrain.getSprite(9, 1);
			hStoneBridge3 = terrain.getSprite(10, 1);
			
			hStreetBridge1 = terrain.getSprite(8, 0);
			hStreetBridge2 = terrain.getSprite(9, 0);
			hStreetBridge3 = terrain.getSprite(10, 0);
			
			/* VERTICAL RIVER CROSSING ANIMATION AND TILES */
			
			vDirtCrossing1 = terrain.getSprite(11, 2);
			vDirtCrossing2 = terrain.getSprite(12, 2);
			vDirtCrossing3 = terrain.getSprite(13, 2);
			
			vStoneBridge1 = terrain.getSprite(11, 1);
			vStoneBridge2 = terrain.getSprite(12, 1);
			vStoneBridge3 = terrain.getSprite(13, 1);
			
			vStreetBridge1 = terrain.getSprite(11, 0);
			vStreetBridge2 = terrain.getSprite(12, 0);
			vStreetBridge3 = terrain.getSprite(13, 0);
			
			System.out.println("Initialized All Bridge Sprites...");
			log.writeToFile("Bridge/Crossing Animations Initialized!...", Log.DEBUG);
			/** END OF ANIMATION RELATED IMAGES **/
			
			concrete1 = terrain.getSprite(0, 4);
			concrete2 = terrain.getSprite(0, 5);
			concrete3 = terrain.getSprite(0, 6);
			metal1 = terrain.getSprite(1, 4);
			metal2 = terrain.getSprite(1, 5);
			
			System.out.println("Initialized All Obstacle Sprites...");
			log.writeToFile("Wall Sprites Initialized!...", Log.DEBUG);
			/** INITIALIZATIONS OF PATHS **/
			dirtPathTurn1 = terrain.getSprite(3, 3);
			dirtPath = terrain.getSprite(3, 4);
			hDirtPath = terrain.getSprite(7, 3);
			dirtPathTurn2 = terrain.getSprite(3, 5);
			dirtPathTurn3 = terrain.getSprite(2, 5);
			dirtPathTurn4 = terrain.getSprite(2, 4);
			dirtCrossroads = terrain.getSprite(3, 6);
			dirtThreewayDown = terrain.getSprite(3, 7);
			dirtThreewayUp = terrain.getSprite(8, 7);
			dirtThreewayRight = terrain.getSprite(8, 5);
			dirtThreewayLeft = terrain.getSprite(9, 5);
			
			streetTurn1 = terrain.getSprite(1, 3);
			streetPath = terrain.getSprite(4, 4);
			hStreetPath = terrain.getSprite(7, 4);
			streetTurn2 = terrain.getSprite(2, 3);
			streetTurn3 = terrain.getSprite(4, 5);
			streetTurn4 = terrain.getSprite(4, 3);
			streetCrossroads = terrain.getSprite(4, 6);
			streetThreewayDown = terrain.getSprite(4, 7);
			streetThreewayUp = terrain.getSprite(7, 7);
			streetThreewayRight = terrain.getSprite(8, 6);
			streetThreewayLeft = terrain.getSprite(9, 6);
			
			stoneTurn1 = terrain.getSprite(5, 3);
			stonePath = terrain.getSprite(5, 4);
			hStonePath = terrain.getSprite(7, 5);
			stoneTurn2 = terrain.getSprite(5, 5);
			stoneTurn3 = terrain.getSprite(6, 5);
			stoneTurn4 = terrain.getSprite(6, 3);
			stoneCrossroads = terrain.getSprite(5, 6);
			stoneThreewayDown = terrain.getSprite(5, 7);
			stoneThreewayUp = terrain.getSprite(6, 7);
			stoneThreewayRight = terrain.getSprite(6, 6);
			stoneThreewayLeft = terrain.getSprite(7, 6);
			
			System.out.println("Intialized All Path Sprites...");
			log.writeToFile("All Path Sprites Initialized!...", Log.DEBUG);
			
			/* Image Arrays for Animations */
			Image[] vRiverAnimated1 = { water11, water12, water13 };
			Image[] vRiverAnimation2 = { water21, water22, water23 };
			Image[] vRiverAnimation3 = { water31, water32, water33 };
			Image[] hRiverAnimation1 = { water41, water42, water43 };
			Image[] hRiverAnimation2 = { water51, water52, water53 };
			Image[] hRiverAnimation3 = { water61, water62, water63 };
			
			Image[] hDCrossing = { hDirtCrossing1, hDirtCrossing2, hDirtCrossing3 };
			Image[] vDCrossing = { vDirtCrossing1, vDirtCrossing2, vDirtCrossing3 };
			Image[] hStone = { hStoneBridge1, hStoneBridge2, hStoneBridge3 };
			Image[] vStone = { vStoneBridge1, vStoneBridge2, vStoneBridge3 };
			Image[] hStreet = { hStreetBridge1, hStreetBridge2, hStreetBridge3 };
			Image[] vStreet = { vStreetBridge1, vStreetBridge2, vStreetBridge3 };
			
			System.out.println("Initialized All Image Arrays (Animation Frames)...");
			log.writeToFile("All Animation Frames Initialized!...", Log.DEBUG);
			
			// Entity Animations (So Far)
			tankToWest = new Animation(tankWest, 300);
			tankToEast = new Animation(tankEast, 300);
			tankToNorth = new Animation(tankNorth, 300);
			tankToSouth = new Animation(tankSouth, 300);
			
			blueToNorth = new Animation(blueN, 300);
			blueToSouth = new Animation(blueS, 300);
			blueToEast = new Animation(blueW, 300);
			blueToWest = new Animation(blueE, 300);
			
			redToNorth = new Animation(redN, 300);
			redToSouth = new Animation(redS, 300);
			redToEast = new Animation(redE, 300);
			redToWest = new Animation(redW, 300);
			
			pinkToNorth = new Animation(pinkN, 300);
			pinkToSouth = new Animation(pinkS, 300);
			pinkToEast = new Animation(pinkE, 300);
			pinkToWest = new Animation(pinkW, 300);
			
			orangeToNorth = new Animation(orangeN, 300);
			orangeToSouth = new Animation(orangeS, 300);
			orangeToEast = new Animation(orangeW, 300);
			orangeToWest = new Animation(orangeE, 300);
			
			System.out.println("Initialized All Entity Animations...");
			
			// Scenery Animations
			
			vRiver1 = new Animation(vRiverAnimated1, 500);
			vRiver2 = new Animation(vRiverAnimation2, 100);
			vRiver3 = new Animation(vRiverAnimation3, 100);
			hRiver1 = new Animation(hRiverAnimation1, 100);
			hRiver2 = new Animation(hRiverAnimation2, 100);
			hRiver3 = new Animation(hRiverAnimation3, 100);
			System.out.println("Initialized All River Animations...");
			
			vStoneBridge = new Animation(vStone, 100);
			hStoneBridge = new Animation(hStone, 100);
			vDirtCrossing = new Animation(vDCrossing, 100);
			hDirtCrossing = new Animation(hDCrossing, 500);
			vStreetBridge = new Animation(vStreet, 100);
			hStreetBridge = new Animation(hStreet, 100);
			System.out.println("Initialized All Bridge Animations...");
			log.writeToFile("Initialized All Animations!...", Log.DEBUG);
			log.writeToFile("------------------FINALIZED ANIMATIONS----------------", Log.INFO);
			
			completeAnimations = true;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(String.format("FAILURE TO INITIALIZE ANIMATIONS at %s : Line 67", "com.treehouseelite.tank.game.handlers.assethandler"));
		}
		if (completeAnimations) {
			System.out.println("All Animations Initialized!...");
			isComplete = true;
			initSounds();
		}
	}
	
	public static void initSounds() {
		
		System.out.println("Initializing Main Menu Music...");
		try {
			titleMus1 = new Music(ResourceLoader.getResource(musPath + "title/titlestart.ogg"));
			titleMus2 = new Music(ResourceLoader.getResource(musPath + "title/title2.ogg"));
			titleMus3 = new Music(ResourceLoader.getResource(musPath + "title/title3.ogg"));
			
			System.out.println("Initialized Main Menu Music!...");
		} catch (SlickException e) {
			e.printStackTrace();
			System.out.println("ERROR: Initializing Main Menu Sounds at " + "com.treehouseelite.tank.game.handlers.AssetHandler" + " : initSounds() Method, First Try/Catch");
		}
		System.out.println("Initializing Loading Screen Music...");
		try {
			loadingScreenMus1 = new Music(ResourceLoader.getResource(musPath + "levels or loading screens/ActionBuilder.ogg"));
			loadingScreenMus2 = new Music(ResourceLoader.getResource(musPath + "levels or loading screens/StruggleforSurvival.ogg"));
			loadingScreenMus3 = new Music(ResourceLoader.getResource(musPath + "levels or loading screens/SurrealSomber.ogg"));
		} catch (SlickException e) {
			e.printStackTrace();
			System.out.println("ERROR: Initializing Loading Screen Sounds at " + "com.treehouseelite.tank.game.handlers.AssetHandler" + " : initSounds() Method, Second Try/Catch");
		}
		initSFX();
		initsComplete();
	}
	
	private static void initsComplete() {
		System.out.println("========================ALL ASSETS INITIALIZED========================");
		try {
			log.writeToFile("==============END OF ASSETS==============", Log.INFO);
		} catch (IOException e) {
		}
	}
	
	public static void initSFX() {
		
		try {
			tankMove = new Sound(ResourceLoader.getResource("res/Sounds/SFX/tankMove.wav"));
		} catch (SlickException e) {
			e.printStackTrace();
		} finally {
			System.out.println("All Sound Effects Initialized...");
		}
	}
	
	private void log(String text, int level) throws IOException {
		log.writeToFile(text, level);
	}
	
	private void log() throws IOException {
		log.nl();
	}
	
	// Getters/Setters
	
	public static final String menuButtonOff = "menuA0";
	public static final String menuButtonOn = "menuA1";
	public static final String grassSparse = "sparseGrass";
	public static final String grassDense = "denseGrass";
	public static final String grassDenser = "denserGrass";
	public static final String c1 = "concrete1";
	public static final String c2 = "concrete2";
	public static final String c3 = "concrete3";
	public static final String m1 = "metal1";
	public static final String m2 = "metal2";
	
	public static org.newdawn.slick.Image getImg(String name) {
		String img = name;
		org.newdawn.slick.Image selectedImg = null;
		
		switch (img) {
			case "menuA0":
				selectedImg = menuButton0;
				break;
			case "menuA1":
				selectedImg = menuButton1;
				break;
			case "sparseGrass":
				selectedImg = sparseGrass;
				break;
			case "denseGrass":
				selectedImg = denseGrass;
				break;
			case "denserGrass":
				selectedImg = denserGrass;
				break;
			case "concrete1":
				selectedImg = concrete1;
				break;
			case "concrete2":
				selectedImg = concrete2;
				break;
			case "concrete3":
				selectedImg = concrete3;
				break;
			case "metal1":
				selectedImg = metal1;
				break;
			case "metal2":
				selectedImg = metal2;
				break;
			default:
				selectedImg = org.newdawn.slick.Image.class.cast(errorImage);
		}
		
		return selectedImg;
	}
	
	/** @deprecated */
	@Deprecated
	@SuppressWarnings("unused")
	public Animation getAnimation(String name) {
		String anim = name;
		Animation selectedAnim = null;
		
		return selectedAnim;
	}
	
}
