package com.treehouseelite.tank.exception;

import java.lang.reflect.Method;

import com.treehouseelite.tank.references.Log;
import com.treehouseelite.tank.references.Util;

public class LogException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7543356760889326304L;
	Class errorClass;
	Method errorMethod;
	Log errorConstructor;
	String errorType;
	
	public static final String LOGCREATION = "OnCeation";
	public static final String LOGWRITING = "OnWriting";
	public static final String LOGDELETION = "OnDelete";
	
	/**
	 * Exception class SPECIFIC to the Central Logging System developed by Matt
	 * Crocco This WILL cause the Game to END, Closing the JVM!
	 * 
	 * @param m
	 *            - Method that throws the exception
	 * @param errorType
	 *            - Error OnCreation, Error OnWriting, Error OnDelete
	 * 
	 */
	public LogException(Method m, String errorType) {
		this.errorClass = m.getClass();
		this.errorMethod = m;
		this.errorType = errorType;
		
		createException(m);
		
	}
	
	/**
	 * Excpetion class SPECIFIC to the Central Logging System developed by Matt
	 * Crocco, This WILL cause the Game to END, Closing the JVM!
	 * 
	 * @param m
	 *            - Constructor that Throws the Exception
	 * @param errorType
	 *            - Error OnCreation, Error OnWriting, Error OnDelete
	 */
	public LogException(Log m, String errorType) {
		this.errorClass = m.getClass();
		this.errorConstructor = m;
		this.errorType = errorType;
		
		createException(m);
		
	}
	
	/**
	 * Responds to a LogException call determining when the crash happened and
	 * where!
	 * 
	 * @param m
	 *            - Method Checker
	 */
	private void createException(Method m) {
		if (errorType.equalsIgnoreCase(LOGCREATION)) {
			String ErrorInfo = String.format("%s -- LogException thrown at Creation!:\n%s --- TYPE: %s\n", Util.getTime(), " at " + errorClass.getPackage().toString() + " --- " + errorMethod.getName(), LOGCREATION);
			System.out.println(ErrorInfo);
		} else if (errorType.equalsIgnoreCase(LOGWRITING)) {
			String ErrorInfo = String.format("%s -- LogException thrown at Writing!:\n%s --- TYPE: %s\n", Util.getTime(), " at " + errorClass.getPackage().toString() + " --- " + errorMethod.getName(), LOGWRITING);
			System.out.println(ErrorInfo);
		} else if (errorType.equalsIgnoreCase(LOGDELETION)) {
			String ErrorInfo = String.format("%s -- LogException thrown at Deletion!:\n%s --- TYPE: %s\n", Util.getTime(), " at " + errorClass.getPackage().toString() + " --- " + errorMethod.getName(), LOGDELETION);
			System.out.println(ErrorInfo);
		} else {
			String ErrorInfo = String.format("%s -- LogException thrown at Unknown Step!:\n%s --- TYPE: %s\n", Util.getTime(), " at " + errorClass.getPackage().toString() + " --- " + errorMethod.getName(), "TYPE UNCLEAR -- CHECK GAME CODE");
			System.out.println(ErrorInfo);
		}
		
		System.exit(0);
	}
	
	/**
	 * Responds to a LogException call determining when the crash happened and
	 * where!
	 * 
	 * @param m
	 *            - Constructor Checker
	 */
	private void createException(Log m) {
		if (errorType.equalsIgnoreCase(LOGCREATION)) {
			String ErrorInfo = String.format("%s -- LogException thrown at Creation!:\n%s --- TYPE: %s\n", Util.getTime(), " at " + errorClass.getPackage().toString() + " --- " + errorMethod.getName(), LOGCREATION);
			System.out.println(ErrorInfo);
		} else if (errorType.equalsIgnoreCase(LOGWRITING)) {
			String ErrorInfo = String.format("%s -- LogException thrown at Writing!:\n%s --- TYPE: %s\n", Util.getTime(), " at " + errorClass.getPackage().toString() + " --- " + errorMethod.getName(), LOGWRITING);
			System.out.println(ErrorInfo);
		} else if (errorType.equalsIgnoreCase(LOGDELETION)) {
			String ErrorInfo = String.format("%s -- LogException thrown at Deletion!:\n%s --- TYPE: %s\n", Util.getTime(), " at " + errorClass.getPackage().toString() + " --- " + errorMethod.getName(), LOGDELETION);
			System.out.println(ErrorInfo);
		} else {
			String ErrorInfo = String.format("%s -- LogException thrown at Unknown Step!:\n%s --- TYPE: %s\n", Util.getTime(), " at " + errorClass.getPackage().toString() + " --- " + errorMethod.getName(), "TYPE UNCLEAR -- CHECK GAME CODE");
			System.out.println(ErrorInfo);
		}
		
		System.exit(0);
	}
}
