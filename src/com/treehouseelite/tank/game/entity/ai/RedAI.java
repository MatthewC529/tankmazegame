package com.treehouseelite.tank.game.entity.ai;

import java.util.Random;

import org.newdawn.slick.geom.Vector2f;

import com.treehouseelite.tank.game.Level;
import com.treehouseelite.tank.game.entity.Enemy;

public class RedAI extends EntityAI {
	
	int LEFT = 0;
	int RIGHT = 1;
	int UP = 2;
	int DOWN = 3;
	int prevDir = -1;
	
	private int numOfMovements = 0;
	
	long timer = 0;
	long waittime = 0;
	
	Vector2f redVector;
	Vector2f targetVector;
	
	float curDistance;
	
	int delta = Level.delta;
	
	public RedAI() {
		
	}
	
	public void start(Enemy entity) {
		
		this.entity = entity;
		
		initAStar();
		System.out.println("Passed aStar initialization");
		redVector = new Vector2f(entity.getOccupiedRect().getCenter());
		targetVector = new Vector2f(EntityAI.targetRect.getCenter());
		curDistance = redVector.distance(targetVector);
		
	}
	
	public void startAStar() {
		
		if (!firstStepsNeeded) {
			boolean happenin = false;
			float distance = redVector.distance(targetVector);
			float distanceDif;
			
			if (timer >= waittime) {
				if (!happenin) {
					
					Random r = new Random();
					int dir = r.nextInt(4);
					
					if (dir != prevDir) {
						move(dir);
						prevDir = dir;
					}
					distanceDif = (redVector.distance(targetVector));
					if (!entity.isColliding() && (distanceDif < distance)) {
						happenin = true;
					} else {
						// System.out.println("Failure :(");
						// System.out.println(Boolean.toString(distanceDif <
						// distance) + " | " +
						// Boolean.toString(entity.isColliding()));
					}
				}
			} else {
				timer = System.currentTimeMillis();
			}
		} else {
			if (timer >= waittime) {
				this.firstSteps();
			} else {
				timer = System.currentTimeMillis();
			}
		}
	}
	
	private void move(int direction) {
		
		float originalX = entity.boundingBox.getX();
		float originalY = entity.boundingBox.getY();
		float width = entity.boundingBox.getWidth();
		float height = entity.boundingBox.getHeight();
		
		System.out.println(direction);
		if (direction == LEFT) {
			entity.boundingBox.setBounds(entity.boundingBox.getX() - 32, entity.boundingBox.getY(), entity.boundingBox.getWidth(), entity.boundingBox.getHeight());
			if (entity.isColliding() && entity.enteringRestricted()) {
				entity.leftCollide = true;
			}
		}
		if (direction == RIGHT) {
			entity.boundingBox.setBounds(entity.boundingBox.getX() + 32, entity.boundingBox.getY(), entity.boundingBox.getWidth(), entity.boundingBox.getHeight());
			if (entity.isColliding()) {
				entity.rightCollide = true;
			}
		}
		if (direction == UP) {
			entity.boundingBox.setBounds(entity.boundingBox.getX(), entity.boundingBox.getY() - 32, entity.boundingBox.getWidth(), entity.boundingBox.getHeight());
			if (entity.isColliding()) {
				entity.upCollide = true;
			}
		}
		
		if ((direction == DOWN) && (numOfMovements < 4)) {
			entity.boundingBox.setBounds(entity.boundingBox.getX(), entity.boundingBox.getY() + 32, entity.boundingBox.getWidth(), entity.boundingBox.getHeight());
			if (entity.isColliding()) {
				entity.downCollide = true;
			}
		}
		
		if (entity.isColliding()) {
			entity.boundingBox.setBounds(originalX, originalY, width, height);
			System.out.println("Enemy Colliding!");
		} else {
			this.pleaseWait();
		}
		numOfMovements++;
	}
	
	private void firstSteps() {
		// TODO
	}
	
	/**
	 * Creates a timer for the entity to wait for, empty parameter creates a
	 * default timer of 5 seconds
	 */
	private void pleaseWait() {
		timer = System.currentTimeMillis();
		waittime = System.currentTimeMillis() + (5 * 1000);
	}
	
	/**
	 * Creates a timer for the entity to wait for, this one can be set to any
	 * amount of seconds.
	 * 
	 * @param seconds
	 *            - Number of seconds for the entity to wait
	 */
	@SuppressWarnings("unused")
	private void pleaseWait(int seconds) {
		timer = System.currentTimeMillis();
		waittime = System.currentTimeMillis() + (seconds * 1000);
	}
	
	/**
	 * Creates a Short Wait timer for the entity to wait for, this one can be
	 * set to wait a certain number of milliseconds.
	 * 
	 * @param milliseconds
	 *            - Integer as the amount of milliseconds to wait for
	 */
	private void pleaseWaitShortly(int milliseconds) {
		timer = System.currentTimeMillis();
		waittime = System.currentTimeMillis() + milliseconds;
	}
}
