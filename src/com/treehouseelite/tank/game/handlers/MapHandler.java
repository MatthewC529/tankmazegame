package com.treehouseelite.tank.game.handlers;

/** 
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 * @Lib LightWeight Java Gaming Library and Slick2D Library
 * 
 * @Thanks Michael Lucido (Best Friend, Alpha Tester, Treehouse Elite)
 * @Thanks Kevin MacLeod (Royalty Free Music)
 * 
 * @License GNU LGPL v3 in Concurrence with GNU GPL v3
 * @Copyright GNU LGPL v3 in Concurrence with GNU GPL v3 
 * 
 * @Warranty 
 *     This file is part of Foobar.
 *
 *  Tank Maze Game is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License in concurrence with the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  Tank Maze Game is distributed in the hope that it will be entertaining,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License and GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License and
 *  GNU Lesser General Public License along with Tank Maze Game.  If not, see <http://www.gnu.org/licenses/>
 **/

import java.util.Random;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

import com.treehouseelite.tank.references.Reference;

//import java.awt.Rectangle;

/**
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 * @deprecated
 * 
 *             A mostly minor class that produces a few needed functions, mostly
 *             deprecated and will likely be replaced.
 */
@Deprecated
public class MapHandler {
	
	public static AssetHandler asset = new AssetHandler();
	
	static Image devMap;
	
	Image map_1;
	
	Image map_2;
	
	Image map_3;
	
	public static Rectangle Map32;
	
	public MapHandler() {
	}
	
	// Randomly Generates a Map ID corresponding to a Level_X.tmx
	// Currently set to 0 for development purposes
	public static int getRandomID() {
		new Random();
		return 0;
		// return id;
	}
	
	/* Create the Rectangle and Grid */
	public static void mapRect() throws SlickException {
		
		System.out.println("Initializing Rectangular Plane...");
		Map32 = new Rectangle(Reference.GUI_WIDTH / 24, Reference.GUI_HEIGHT / 24, 800, 640);
		System.out.println("Map32 Initialized!...");
	}
	
}
