package com.treehouseelite.tank.game.handlers;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Font;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class OutputHandler {
	
	static Font FONT;
	
	public static void initFont() throws SlickException {
		AngelCodeFont font;
		
		Image fontImg = new Image("res/Font/TMZGlyphs_00.png");
		font = new AngelCodeFont("res/Font/TMZGlyphs.fnt", fontImg);
		
		FONT = font;
	}
	
	// Lots of different write functions for different data types
	public static void write(String text, int x, int y) {
		FONT.drawString(x, y, text);
	}
	
	public void write(String text, double x, double y) {
		FONT.drawString((float) x, (float) y, text);
	}
	
	public static void write(String text, float x, float y) {
		
		FONT.drawString(x, y, text);
	}
	
	public void write(String text, short x, short y) {
		FONT.drawString(x, y, text);
	}
}
