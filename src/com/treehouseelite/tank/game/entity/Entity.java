package com.treehouseelite.tank.game.entity;

import java.awt.Point;
import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Sound;
import org.newdawn.slick.geom.Rectangle;

import com.treehouseelite.tank.game.Level;
import com.treehouseelite.tank.game.handlers.TileHandler;
import com.treehouseelite.tank.references.Reference;

public class Entity {
	
	int x;
	int y;
	
	public int curRectArrayX;
	public int curRectArrayY;
	
	String teleportedFrom;
	
	public Rectangle boundingBox;
	public Rectangle occupiedRect;
	public Rectangle curRect;
	
	public static short projectileNum = 0;
	
	boolean notMoving = false;
	boolean wasStopped = false;
	boolean reloaded = true;
	Animation curAnimation;
	
	String curDirection;
	
	public static Rectangle[] teleportingTiles = { Reference.tiledLayer[0][9], Reference.tiledLayer[0][10], Reference.tiledLayer[24][9], Reference.tiledLayer[24][10] };
	public static Rectangle[] restrictedTiles = { Reference.tiledLayer[12][9], Reference.tiledLayer[12][10], Reference.tiledLayer[11][10], Reference.tiledLayer[13][10], Reference.tiledLayer[12][8], Reference.tiledLayer[12][7] };
	
	Sound tankMove;
	
	public static ArrayList<Entity> entityList = new ArrayList<Entity>();
	public static ArrayList<Rectangle> entityBB = new ArrayList<Rectangle>();
	
	public void drawEntity(Entity e) {
		
		if (!(e instanceof Projectile)) {
			Graphics g = new Graphics();
			g.drawAnimation(e.curAnimation, e.boundingBox.getX() - 4, e.boundingBox.getY() - 4);
			e.curAnimation.update(Level.delta);
		} else {
			drawProjectile((Projectile) e);
		}
	}
	
	protected Rectangle getBoundingBox(Rectangle r) {
		
		Rectangle bounding;
		bounding = new Rectangle(r.getX() + 4, r.getY() + 2, r.getWidth() - 8, r.getHeight() - 9);
		entityBB.add(bounding);
		return bounding;
		
	}
	
	public void die() {
		/*
		 * if(e instanceof Player) { Reference.GameOver = true;
		 * this.curAnimation = playerDeath; }else { this.curAnimation =
		 * getDeath(this.id);
		 */
		if ((this instanceof Player) && !Reference.laserTankCode) {
			
		} else {
			
		}
		System.out.println("Player has Died!");
	}
	
	/**
	 * Determines if the Entity is colliding with an Object or Other Entity
	 * (Projectile or Enemy)
	 * 
	 * @category Boolean getter
	 * @return Returns a boolean of whether or not a Collision is occurring so
	 *         that collision handling can be completed
	 * */
	public boolean isColliding() {
		boolean collision = false;
		
		for (int i = 0; i < TileHandler.collisionTiles.size(); i++) {
			if (this.boundingBox.intersects(TileHandler.collisionTiles.get(i)) || (this.boundingBox.getMinX() <= Reference.tiledLayer[0][0].getMinX()) || (this.boundingBox.getMaxX() >= Reference.tiledLayer[24][0].getMaxX())) {
				collision = true;
				break;
			} else {
				collision = false;
			}
		}
		
		for (int x = 0; x < entityBB.size(); x++) {
			if (this.boundingBox.intersects(entityBB.get(x)) && !this.boundingBox.equals(entityBB.get(x))) {
				collision = true;
				if (this instanceof Player) {
					this.die();
				}
			}
		}
		
		for (int c = 0; c < Projectile.activeProjectilesBB.size(); c++) {
			if (this.boundingBox.intersects(Projectile.activeProjectilesBB.get(c))) {
				collision = true;
				if (this instanceof Player) {
					Projectile.activeProjectiles.get(c).explode();
					this.die();
				}
				if (this instanceof Enemy) {
					if (Reference.PLAYER_ARMED) {
						this.die();
						collision = true;
					} else {
						collision = true;
					}
				}
			}
		}
		
		return collision;
	}
	
	public boolean enteringRestricted() {
		boolean restricted = false;
		
		for (int g = 0; g < restrictedTiles.length; g++) {
			Rectangle curRect = restrictedTiles[g];
			if (curRect.intersects(this.boundingBox)) {
				restricted = true;
				break;
			} else {
				;
			}
		}
		
		return restricted;
	}
	
	/**
	 * Checks if a entity is inside a "Teleporting" tile (of 4 on map)
	 * 
	 * @return Boolean of whether or not the Bounding Box intersects a
	 *         Teleporting Tile
	 */
	public boolean isTeleporting() {
		boolean isDoing = false;
		
		for (int count = 0; count < teleportingTiles.length; count++) {
			if (this.boundingBox.intersects(teleportingTiles[count])) {
				
				if (teleportingTiles[count].equals(teleportingTiles[0]) || teleportingTiles[count].equals(teleportingTiles[1])) {
					// Animation LeavingLeft
					this.teleportedFrom = "Left";
				} else {
					// AnimationLeavingRight
					this.teleportedFrom = "Right";
				}
				isDoing = true;
			}
		}
		
		return isDoing;
	}
	
	public void drawProjectile(Projectile e) {
		Graphics artist = new Graphics();
		
		artist.setColor(Color.cyan);
		artist.fillRect(e.pBoundary.getX(), e.pBoundary.getY(), e.pBoundary.getWidth(), e.pBoundary.getHeight());
		
	}
	
	public void reload() {
		
	}
	
	protected void checkCurTile() {
		
		for (int i = 0; i < Reference.tiledLayer.length; i++) {
			for (int y = 0; y < Reference.tiledLayer[0].length; y++) {
				
				Rectangle rect = Reference.tiledLayer[i][y];
				Point p = new Point((int) this.boundingBox.getCenterX(), (int) this.boundingBox.getCenterY());
				
				if (rect.contains(p.x, p.y)) {
					this.curRect = rect;
					this.occupiedRect = rect;
					this.curRectArrayX = i;
					this.curRectArrayY = y;
				}
				
			}
		}
		
	}
	
	// getters
	public boolean isReloaded() {
		return reloaded;
	}
	
	public Rectangle getBoundingBox() {
		return boundingBox;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public String getCurrentDirection() {
		return curDirection;
	}
	
	public boolean isMoving() {
		boolean moving;
		if (notMoving) {
			moving = false;
		}
		if (!notMoving) {
			moving = true;
		} else {
			moving = notMoving;
		}
		
		return moving;
	}
	
	public Rectangle getOccupiedRect() {
		return occupiedRect;
	}
	
	public int getCurRectArrayX() {
		return curRectArrayX;
	}
	
	public int getCurRectArrayY() {
		return curRectArrayY;
	}
}
