package com.treehouseelite.tank.testbed;

import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.TrueTypeFont;

public class GameTextField {
	
	private float x;
	private float y;
	private float width;
	private float height;
	
	private Font defFont = new TrueTypeFont(new java.awt.Font("Courier New", 14, java.awt.Font.ITALIC), true);
	
	public GameTextField(GameContainer gc, float x, float y, float width, float height) {
		this.setX(x);
		this.setY(y);
		this.setWidth(width);
		this.setHeight(height);
	}
	
	/* GETTERS AND SETTERS */
	
	public Font getFont() {
		return defFont;
	}
	
	public void setFont(Font fnt) {
		defFont = fnt;
	}
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public float getWidth() {
		return width;
	}
	
	public void setWidth(float width) {
		this.width = width;
	}
	
	public float getHeight() {
		return height;
	}
	
	public void setHeight(float height) {
		this.height = height;
	}
	
}
