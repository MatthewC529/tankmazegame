package com.treehouseelite.tank.references;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import com.treehouseelite.tank.exception.LogException;

/**
 * @author Matthew Crocco (Treehouse Elite)
 * @thanks Michael Lucido (Best Friend, Treehouse Elite, Tester) Nathyn Vega
 *         (Alpha Tester, Supporter) Kevin MacLeod (Royalty Free Music)
 * 
 * @copyright Licensed Under the GNU LGPL v3 in Concurrence with the GPL v3
 * 
 * @warranty This file is part of Tank Maze Game.
 * 
 *           Tank Maze Game is free software: you can redistribute it and/or
 *           modify it under the terms of the GNU Lesser General Public License
 *           in concurrence with the GNU General Public License as published by
 *           the Free Software Foundation, either version 3 of the License, or
 *           any later version.
 * 
 *           Tank Maze Game is distributed in the hope that it will be
 *           entertaining, but WITHOUT ANY WARRANTY; without even the implied
 *           warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *           See the GNU Lesser General Public License and GNU General Public
 *           License for more details.
 * 
 *           You should have received a copy of the GNU General Public License
 *           and GNU Lesser General Public License along with Tank Maze Game. If
 *           not, see <http://www.gnu.org/licenses/>
 */

/**
 * A Central Logging System for any game or software -- Early
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * @note Early Development
 */
public class Log {
	
	public static final int INFO = 0;
	public static final int DEBUG = 1;
	public static final int ERROR = 2;
	public static final int EMPTY = -2;
	public static final int SUPERFLUOUS = -1;
	
	private static final int BLANK = -5297;
	
	private static int LogCounter;
	
	private static final String logPath = System.getProperty("user.dir") + "\\logs\\log";
	
	private static BufferedReader MorganFreeman;
	private static BufferedWriter TomClancy;
	BufferedWriter WillLangille;
	BufferedReader BillyLangille;
	
	private static File logFile;
	private static File directory = new File("res");
	
	/**
	 * A Central Log System, this is the Initializer for the File, Use the other
	 * constructor in other classes!
	 * 
	 * @param id
	 *            - A numerical id for the log file
	 * @throws IOException
	 */
	public Log(long id) throws IOException {
		
		Random r = new Random();
		long ranExtra = System.currentTimeMillis() / r.nextInt(52997);
		
		int AmountOfLogFiles = getNumLogs();
		LogCounter = getLogCount();
		if (AmountOfLogFiles >= 3) {
			deletePreviousLogFile();
			AmountOfLogFiles--;
		}
		logFile = new File(String.format("%s%s.telog", logPath, "#" + Integer.toString(LogCounter) + " -- " + Long.toString(ranExtra)));
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
				assign();
			} catch (IOException e) {
				e.printStackTrace();
				try {
					this.writeToFile(e.getMessage(), ERROR);
				} catch (IOException e1) {
				}
			}
		} else {
			assign();
		}
		
	}
	
	/**
	 * A Central Log System, This Constructor is for Non-Initializers, USE IF
	 * AND ONLY IF YOU HAVE INITIALIZED THE LOGFILE!
	 * 
	 * @throws LogException
	 *             - Dont Worry About This... IT WILL CRASH THE GAME!
	 */
	public Log() throws LogException {
		if (!hasLogFile())
			throw new LogException(this, "OnCreation");
	}
	
	/**
	 * Checks if the LogFile at the CURRENT LogPath exists
	 * 
	 * @return LogFile Existance Boolean
	 */
	public boolean hasLogFile() {
		return logFile.exists();
	}
	
	/**
	 * Returns the Absolute Path of the LogFile
	 * 
	 * @return Absolute Path of LogFile
	 */
	public String getCurrentLogPath() {
		return logFile.getAbsolutePath();
	}
	
	/**
	 * Re-assign the log directory through a custom path
	 * 
	 * @param newPath
	 *            - New path for the log file in the format
	 *            "yourdirectoryhere/log", THATS IT, the rest is done for you
	 * @param deletePreviousLog
	 *            - Do you want to delete the previous log file?
	 */
	public void setCurrentLogPath(String newPath, boolean deletePreviousLog) {
		if (deletePreviousLog) {
			try {
				logFile.deleteOnExit();
			} catch (Exception e) {
				new LogException(this, "OnDelete");
				try {
					this.writeToFile(e.getMessage(), ERROR);
				} catch (IOException e1) {
				}
			}
		}
		
		logFile = new File(newPath);
		
		if (logFile.exists()) {
			
		} else {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				try {
					this.writeToFile(e.getMessage(), ERROR);
				} catch (IOException e1) {
				}
				new LogException(this, "OnCreation");
			}
		}
		
		assign();
		
	}
	
	/**
	 * Very simialr to the other writeToFile method but is used for plain
	 * strings instead of InputStream's, Write string to log file
	 * 
	 * @param ErrorText
	 *            - Information to write to the log file
	 * @param TextImportance
	 *            - Level of importance of the text, refer to the Log int values
	 *            to see which integers correspond to each level
	 * @throws IOException
	 */
	public void writeToFile(String ErrorText, int TextImportance) throws IOException {
		String level;
		String time = Util.getTime();
		String ErrText = "ERROR WHEN PRINTING LOG TEXT";
		
		if (TextImportance != BLANK) {
			
			if (TextImportance == -1) {
				level = "SUPERFLUOUS";
			} else if (TextImportance == 0) {
				level = "INFO";
			} else if (TextImportance == 1) {
				level = "DEBUG";
			} else if (TextImportance == 2) {
				level = "ERROR";
			} else if (TextImportance == -2) {
				level = "";
			} else {
				level = "FATAL/UNKNOWN";
			}
			
			ErrText = String.format("%s_%s -- %s", time, level, ErrorText.toString());
			
		} else {
			ErrText = "";
		}
		System.out.println(ErrText);
		
		TomClancy.write(ErrText);
		TomClancy.newLine();
		TomClancy.flush();
		
	}
	
	public void nl() throws IOException {
		writeToFile("", Log.BLANK);
	}
	
	/**
	 * Very similar to the other writeToFile method, allows the use of
	 * InputStreams to write to log files
	 * 
	 * @param ErrorText
	 *            - Information to write to the log file
	 * @param TextImportance
	 *            - Importance level, refer to the Log Integers for what ints
	 *            correspond to each level
	 * @throws IOException
	 */
	public void writeToFile(InputStream ErrorText, int TextImportance) throws IOException {
		
		String level;
		String time = Util.getTime();
		
		if (TextImportance == -1) {
			level = "SUPERFLUOUS";
		} else if (TextImportance == 0) {
			level = "INFO";
		} else if (TextImportance == 1) {
			level = "DEBUG";
		} else if (TextImportance == 2) {
			level = "ERROR";
		} else {
			level = "FATAL/UNKNOWN";
		}
		
		String ErrText = String.format("%s_%s -- %s", time, level, ErrorText.toString());
		TomClancy.write(ErrText);
		TomClancy.flush();
		
	}
	
	/**
	 * Assigns the BufferedReader and BufferedWriter to the CURRENT Log File, a
	 * Re-Assign will occur if the log path is changed with setCurrentLogPath
	 * 
	 * @author Matthew Crocco (Treehouse Elite)
	 * 
	 */
	private void assign() {
		
		try {
			TomClancy = new BufferedWriter(new FileWriter(logFile.getAbsoluteFile()));
			MorganFreeman = new BufferedReader(new FileReader(logFile.getAbsoluteFile()));
			MorganFreeman.mark(9999);
		} catch (IOException e) {
			e.printStackTrace();
			new LogException(this, LogException.LOGWRITING);
		}
		
		try {
			this.writeToFile("Tom Clancy and Morgan Freeman, Signing On...", SUPERFLUOUS);
			this.writeToFile("Game Mode: " + Reference.getGameMode(), INFO);
			TomClancy.newLine();
		} catch (IOException e) {
			e.printStackTrace();
			
			try {
				this.writeToFile(e.getMessage(), Log.ERROR);
			} catch (IOException e1) {
			}
		}
	}
	
	/**
	 * Closes all necessary variables and allows for a nice, easy closing... OR
	 * it decimates the ending sequence by throwing a LogException
	 * 
	 * @throws LogException
	 */
	public void finishUp() throws LogException {
		try {
			TomClancy.close();
			MorganFreeman.close();
			BillyLangille.close();
			WillLangille.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new LogException(this, "OnClosing");
		}
	}
	
	/**
	 * Gets the Current Log text in the form of a String
	 * 
	 * @return The ENTIRE Log in a formatted string
	 * @author Matthew Crocco (Treehouse Elite)
	 * @throws IOException
	 */
	public String getCurrentLog() throws IOException {
		String text = null;
		String prevText = null;
		boolean once = true;
		
		int fileLength = com.treehouseelite.tank.references.Util.getFileLength(logFile.getAbsolutePath());
		for (int i = 0; i < fileLength; i++) {
			if (once) {
				text = MorganFreeman.readLine() + "\n";
				once = false;
			} else {
				text = String.format("%s %s\n", prevText, MorganFreeman.readLine());
			}
			prevText = text;
		}
		text = prevText;
		
		MorganFreeman.reset();
		
		if (text == null)
			return text = "No Log Text Found";
		else
			return text;
	}
	
	/**
	 * Prints current telog information to the console
	 * 
	 * @author Matthew Crocco (Treehouse Elite)
	 * @throws IOException
	 */
	public void printCurrentLog() throws IOException {
		String text = null;
		String prevText = null;
		boolean once = true;
		
		int fileLength = com.treehouseelite.tank.references.Util.getFileLength(logFile.getAbsolutePath());
		for (int i = 0; i < fileLength; i++) {
			if (once) {
				text = MorganFreeman.readLine() + "\n";
				once = false;
			} else {
				text = String.format("%s %s\n", prevText, MorganFreeman.readLine());
			}
			prevText = text;
		}
		text = prevText;
		MorganFreeman.reset();
	}
	
	/**
	 * Returns the current Log number to prevent conflict and keep THREE logs
	 * only. IF no counter file exists than a new one is created.
	 * 
	 * @return Current Log Number from counter.txt
	 * @throws IOException
	 */
	public int getLogCount() throws IOException {
		File logDirectory = new File(System.getProperty("user.dir") + "\\logs");
		File logCounter = new File(System.getProperty("user.dir") + "\\logs\\COUNTER.track");
		
		if (!logDirectory.exists()) {
			logDirectory.mkdir();
		}
		
		if (logCounter.exists()) {
			BillyLangille = new BufferedReader(new FileReader(logCounter));
			String current = BillyLangille.readLine();
			int currentCount = Integer.parseInt(current);
			logCounter.delete();
			logCounter.createNewFile();
			WillLangille = new BufferedWriter(new FileWriter(logCounter));
			WillLangille.write(Integer.toString(currentCount + 1));
			WillLangille.close();
			return currentCount;
		} else {
			logCounter.createNewFile();
			WillLangille = new BufferedWriter(new FileWriter(logCounter));
			WillLangille.write(Integer.toString(0));
			WillLangille.close();
			return getLogCount();
		}
		
	}
	
	/**
	 * Deletes the least recent log file after 3 exist
	 * 
	 * @author Matthew Crocco (Treehouse Elite)
	 * @see Log
	 */
	private void deletePreviousLogFile() {
		if (directory.isDirectory()) {
			File[] directoryFiles = directory.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					boolean accepted = false;
					if (name.contains("log") && name.endsWith(".telog")) {
						accepted = true;
					}
					return accepted;
					
				}
			});
			int logc = LogCounter - 3;
			for (int i = 0; i < directoryFiles.length; i++) {
				if (directoryFiles[i].getName().contains(Integer.toString(logc))) {
					directoryFiles[i].delete();
				}
			}
		}
	}
	
	/**
	 * Returns the number of log files in the log directory to determine whether
	 * any logs should be deleted
	 * 
	 * @return Number of Log Files in the Assigned Log Directory
	 * @author Matthew Crocco (Treehouse Elite)
	 * @see Log
	 */
	private int getNumLogs() {
		int logNum = 0;
		
		if (directory.isDirectory()) {
			File[] directoryFiles = directory.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					boolean accepted = false;
					if (name.contains("log") && name.endsWith(".telog")) {
						accepted = true;
					}
					return accepted;
					
				}
			});
			logNum = directoryFiles.length;
		}
		System.out.println(logNum);
		return logNum;
	}
	
}
