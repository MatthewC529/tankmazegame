package com.treehouseelite.tank.references;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

import com.treehouseelite.tank.game.handlers.MapHandler;
import com.treehouseelite.tank.game.handlers.TileHandler;

/**
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 *         Michael Lucido (Best Friend, Treehouse Elite, Tester) Nathyn Vega
 *         (Alpha Tester, Supporter) Kevin MacLeod (Royalty Free Music)
 * 
 *         Licensed Under the GNU LGPL v3 in Concurrence with the GPL v3
 * 
 *         This file is part of Tank Maze Game.
 * 
 *         Tank Maze Game is free software: you can redistribute it and/or
 *         modify it under the terms of the GNU Lesser General Public License in
 *         concurrence with the GNU General Public License as published by the
 *         Free Software Foundation, either version 3 of the License, or any
 *         later version.
 * 
 *         Tank Maze Game is distributed in the hope that it will be
 *         entertaining, but WITHOUT ANY WARRANTY; without even the implied
 *         warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 *         the GNU Lesser General Public License and GNU General Public License
 *         for more details.
 * 
 *         You should have received a copy of the GNU General Public License and
 *         GNU Lesser General Public License along with Tank Maze Game. If not,
 *         see <http://www.gnu.org/licenses/>
 **/
/**
 * A Reference class for Global Flags and Common Methods
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 */
@SuppressWarnings("deprecation")
public class Reference {
	
	/** GLOBAL FLAGS */
	
	public static boolean GAMEOVER = false;
	public static boolean PLAYER_ARMED = false;
	
	/*************************/
	
	public static boolean debug = true;
	public static boolean reloadOn = true;
	public static boolean laserTankCode = false;
	
	public static boolean playerPlacementDone = false;
	
	public static final String TITLE = "Tank Maze Game";
	public static final String VERSION = "pre-Alpha v0.0.7b";
	public static final String BUILD = "014";
	public static final String AUTHOR = "Matthew Crocco";
	
	public static final String DPATH = "res/World/level_";
	public static final String iPath16 = "res/Images/icon/icon16.png";
	public static final String iPath32 = "res/Images/icon/icon32.png";
	
	public static final String[] gameIcons = { iPath16, iPath32 };
	
	/* GAME STATES/SCREENS */
	public static final int MAIN_MENU = 0, OPTIONS = 1, PLAY = 2, ROUND_RESULTS = 3, CINEMATICS = 4;
	
	/* GUI VALUES */
	public static final int GUI_WIDTH = 876;
	public static final int GUI_HEIGHT = 696;
	public static final int MAP_X = GUI_WIDTH / 30;
	public static final int MAP_Y = GUI_HEIGHT / 32;
	
	/* Constants */
	public static final int TILE_SIZE = 31;
	public static final int ALPHA_SIZE = 16;
	public static final int NUMERAL_SIZE = 16;
	
	public static final long LogCount = 0;
	
	public static float defProjectileVelocity;
	public static float velCoEfficient = 0.1545f;
	public static float aeroDragConstant = 0.000015f;
	
	private static TileHandler tile = new TileHandler();
	public static Rectangle Map32 = new Rectangle(Reference.GUI_WIDTH / 24, Reference.GUI_HEIGHT / 24, 800, 640);
	public static Rectangle[][] tiledLayer;
	
	public static String getGameMode() {
		if (Reference.debug)
			return "DEBUG";
		else if (!Reference.debug)
			return "NORMAL";
		else
			return "UNKNOWN/ERROR";
	}
	
	public static boolean isDebug() {
		return debug;
	}
	
	public void setDebug(boolean debug) {
		Reference.debug = debug;
	}
	
	public static void initializeTileLayer() throws SlickException {
		tiledLayer = tile.createTiles(MapHandler.getRandomID(), Map32);
	}
}
