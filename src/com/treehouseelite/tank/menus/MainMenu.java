package com.treehouseelite.tank.menus;

/** 
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 * @Lib LightWeight Java Gaming Library and Slick2D Library
 * 
 * @Thanks Michael Lucido (Best Friend, Alpha Tester, Treehouse Elite)
 * @Thanks Kevin MacLeod (Royalty Free Music)
 * 
 * @License GNU LGPL v3 in Concurrence with GNU GPL v3
 * @Copyright GNU LGPL v3 in Concurrence with GNU GPL v3 
 * 
 * @Warranty 
 *     This file is part of Foobar.
 *
 *  Tank Maze Game is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License in concurrence with the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  Tank Maze Game is distributed in the hope that it will be entertaining,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License and GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License and
 *  GNU Lesser General Public License along with Tank Maze Game.  If not, see <http://www.gnu.org/licenses/>
 **/

import java.io.IOException;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.treehouseelite.tank.TankMaze;
import com.treehouseelite.tank.exception.LogException;
import com.treehouseelite.tank.game.Level;
import com.treehouseelite.tank.game.handlers.AssetHandler;
import com.treehouseelite.tank.game.handlers.OutputHandler;
import com.treehouseelite.tank.references.Log;
import com.treehouseelite.tank.references.Reference;
import com.treehouseelite.tank.references.Util;

public class MainMenu extends BasicGameState {
	
	/* Instantiations */
	
	/* VARIABLES */
	Rectangle startButton;
	float MouseX = 0;
	float MouseY = 0;
	
	private boolean mus1ToBePlayed = true;
	private boolean mus2Played = false;
	private static Log log;
	
	int mapID;
	
	float menuButtonsX = 100;
	float playY = 200;
	float settingsY = 320;
	float exitY = 440;
	
	static float buttonWidth = 326;
	static float buttonHeight = 100;
	
	static float menuButtonsWordsX = 120;
	static float playWordsY = 240;
	static float settingsWordsY = 360;
	static float exitWordsY = 480;
	
	static boolean playHover = false;
	static boolean settingsHover = false;
	static boolean exitHover = false;
	
	public Image buttonA0;
	public Image buttonA1;
	
	AssetHandler asset = new AssetHandler();
	
	public MainMenu(int id) {
		
	}
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		
		log = TankMaze.log;
		
		if (!Reference.debug) {
			AssetHandler.initSounds();
		}
		
		try {
			log.writeToFile(String.format("%s - %s", this.getClass().getName(), "Sounds Initialized!"), Log.INFO);
			
			System.out.println("IS IT WORKING?");
			System.out.println(log.getCurrentLog());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				log.writeToFile(e.getMessage(), Log.ERROR);
			} catch (IOException e1) {
			}
		}
		
		obtainImages();
	}
	
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.setAntiAlias(true);
		
		drawMainMenu(container, g);
	}
	
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		Input input = container.getInput();
		MouseX = input.getMouseX();
		MouseY = input.getMouseY();
		
		if (!Reference.debug) {
			playMusic();
		}
		if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
			if (((MouseX >= menuButtonsX) && (MouseX <= (menuButtonsX + buttonWidth)) && ((MouseY >= playY) && (MouseY <= (playY + buttonHeight))))) {
				Level.actuallyCalled = true;
				game.getState(Reference.PLAY).init(container, game);
				game.enterState(Reference.PLAY);
				
				if (!Reference.debug) {
					if (AssetHandler.titleMus1.playing()) {
						AssetHandler.titleMus1.fade(3500, 0, true);
					} else if (AssetHandler.titleMus2.playing()) {
						AssetHandler.titleMus2.fade(3500, 0, true);
					} else if (AssetHandler.titleMus3.playing()) {
						AssetHandler.titleMus3.fade(3500, 0, true);
					}
				}
			} else if (((MouseX >= menuButtonsX) && (MouseX <= (menuButtonsX + buttonWidth)) && ((MouseY >= settingsY) && (MouseY <= (settingsY + buttonHeight))))) {
				game.enterState(Reference.OPTIONS);
			} else if (((MouseX >= menuButtonsX) && (MouseX <= (menuButtonsX + buttonWidth)) && ((MouseY >= exitY) && (MouseY <= (exitY + buttonHeight))))) {
				try {
					Util.conductFinalization();
				} catch (LogException e) {
					e.printStackTrace();
				}
				System.exit(0);
			}
		}
		
		if (((MouseX >= menuButtonsX) && (MouseX <= (menuButtonsX + buttonWidth)) && ((MouseY >= playY) && (MouseY <= (playY + buttonHeight))))) {
			playHover = true;
		} else if (((MouseX >= menuButtonsX) && (MouseX <= (menuButtonsX + buttonWidth)) && ((MouseY >= settingsY) && (MouseY <= (settingsY + buttonHeight))))) {
			settingsHover = true;
		} else if (((MouseX >= menuButtonsX) && (MouseX <= (menuButtonsX + buttonWidth)) && ((MouseY >= exitY) && (MouseY <= (exitY + buttonHeight))))) {
			exitHover = true;
		} else {
			playHover = false;
			settingsHover = false;
			exitHover = false;
		}
	}
	
	@Override
	public int getID() {
		
		return 0;
	}
	
	private void drawMainMenu(GameContainer gc, Graphics g) {
		
		g.setAntiAlias(true);
		
		Image play = buttonA0;
		Image settings = buttonA0;
		Image exit = buttonA0;
		
		if (playHover) {
			play = buttonA1;
		}
		if (settingsHover) {
			settings = buttonA1;
		}
		if (exitHover) {
			exit = buttonA1;
		}
		
		g.drawImage(play, menuButtonsX, playY);
		g.drawImage(settings, menuButtonsX, settingsY);
		g.drawImage(exit, menuButtonsX, exitY);
		
		if (Reference.debug) {
			OutputHandler.write("Enter Battle", menuButtonsWordsX, playWordsY);
			OutputHandler.write("Commanders Tools", menuButtonsWordsX, settingsWordsY);
			OutputHandler.write("Go On Home Leave", menuButtonsWordsX, exitWordsY);
			OutputHandler.write("MouseX : " + Float.toString(MouseX), 10, 20);
			OutputHandler.write("MouseY : " + Float.toString(MouseY), 10, 40);
			OutputHandler.write("PlayX : " + Float.toString(menuButtonsX), 10, 60);
			OutputHandler.write("playY : " + Float.toString(playY), 10, 80);
		}
	}
	
	private void playMusic() {
		if (!Reference.debug) {
			if (mus1ToBePlayed) {
				AssetHandler.titleMus1.play();
				System.out.println("Music1 Playing");
				mus1ToBePlayed = false;
			}
			
			if (!AssetHandler.titleMus1.playing() && !AssetHandler.titleMus2.playing() && !AssetHandler.titleMus3.playing() && !mus2Played) {
				AssetHandler.titleMus2.play();
				mus2Played = true;
				System.out.println("Music2 Playing");
			} else if (!AssetHandler.titleMus2.playing() && mus2Played) {
				AssetHandler.titleMus3.play();
				System.out.println("Music3 Playing");
				mus2Played = false;
				mus1ToBePlayed = true;
			}
		}
	}
	
	private void obtainImages() {
		buttonA0 = AssetHandler.getImg(AssetHandler.menuButtonOff);
		buttonA1 = AssetHandler.getImg(AssetHandler.menuButtonOn);
	}
}
