package com.treehouseelite.tank.game.entity;

import java.util.ArrayList;

import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;

import com.treehouseelite.tank.game.Level;
import com.treehouseelite.tank.game.handlers.TileHandler;
import com.treehouseelite.tank.references.Reference;

public class Projectile extends Entity {
	
	// TODO TEST PROJECTILES + ADD FIRING TO PLAYER FOR NOW!
	
	public float startX;
	public float startY;
	public float halfDeltaX;
	public float halfDeltaY;
	public float velocity;
	
	private String directionToTravel;
	
	public static ArrayList<Projectile> activeProjectiles = new ArrayList<Projectile>();
	public static ArrayList<Rectangle> activeProjectilesBB = new ArrayList<Rectangle>();
	
	Circle explosionRadius;
	
	Rectangle pBoundary;
	
	private float velocityCoEfficient = Reference.velCoEfficient;
	private float dragConstant = Reference.aeroDragConstant;
	
	public Projectile() {
	}
	
	public Projectile(Rectangle BoundingBox, String direction) {
		halfDeltaX = (BoundingBox.getMaxX() - BoundingBox.getMinX()) / 2;
		halfDeltaY = (BoundingBox.getMaxY() - BoundingBox.getMinY()) / 2;
		
		directionToTravel = new String(direction);
		startX = BoundingBox.getMinX();
		startY = BoundingBox.getMaxY() - halfDeltaY;
		
		setStarts(BoundingBox);
		
		activeProjectiles.add(this);
		Entity.entityList.add(this);
		
		this.getProjectileBoundary();
		this.boundingBox = this.pBoundary;
		
		velocity = Reference.defProjectileVelocity;
		
	}
	
	public void update() {
		if (this.hittingWall()) {
			this.explode();
		} else {
			;
		}
		
		moveProjectile();
		this.boundingBox = this.pBoundary;
		this.applyAeroDrag();
		if (this.isColliding()) {
			this.explode(); // TODO
		}
		
	}
	
	public void explode() {
		// this.curAnimation = AssetHandler.explosion;
		// setExplosionRadius();
		activeProjectiles.remove(this);
		Entity.entityList.remove(this);
		this.pBoundary = null;
	}
	
	@Override
	public float getX() {
		return startX;
	}
	
	public float getCurrentVelocity() {
		return velocity;
	}
	
	public String getDirection() {
		return directionToTravel;
	}
	
	public void applyAeroDrag() {
		velocity = velocityCoEfficient * Level.delta;
		velocityCoEfficient -= dragConstant;
	}
	
	public void getProjectileBoundary() {
		Rectangle bounds;
		if (directionToTravel.equalsIgnoreCase("North") || directionToTravel.equalsIgnoreCase("South")) {
			bounds = new Rectangle(startX, startY, 3, 8);
		} else {
			bounds = new Rectangle(startX, startY, 8, 3);
		}
		
		this.pBoundary = bounds;
	}
	
	private void setStarts(Rectangle bb) {
		
		if (directionToTravel.equalsIgnoreCase("North") || directionToTravel.equalsIgnoreCase("South")) {
			startX = bb.getMaxX() - halfDeltaX - 1;
			if (directionToTravel.equalsIgnoreCase("North")) {
				startY = bb.getMinY() - 8;
			} else {
				startY = bb.getMaxY() + 8;
			}
		} else if (directionToTravel.equalsIgnoreCase("East")) {
			startX = bb.getMaxX();
			startY = bb.getMaxY() - halfDeltaY - 2;
		} else {
			startX = bb.getMinX() - 9;
			startY = bb.getMaxY() - halfDeltaY - 1;
		}
	}
	
	public boolean hittingWall() {
		boolean wall = false;
		
		for (int i = 0; i < TileHandler.collisionTiles.size(); i++) {
			if (this.pBoundary.intersects(TileHandler.collisionTiles.get(i))) {
				wall = true;
			} else {
				wall = false;
			}
		}
		
		return wall;
	}
	
	public void moveProjectile() {
		float x = pBoundary.getX();
		float y = pBoundary.getY();
		if (this.directionToTravel.equalsIgnoreCase("North")) {
			this.pBoundary.setLocation(x, y - velocity);
		} else if (this.directionToTravel.equalsIgnoreCase("South")) {
			this.pBoundary.setLocation(x, y + velocity);
		} else if (this.directionToTravel.equalsIgnoreCase("East")) {
			this.pBoundary.setLocation(x + velocity, y);
		} else if (this.directionToTravel.equalsIgnoreCase("West")) {
			this.pBoundary.setLocation(x - velocity, y);
		}
	}
	
	public void drawProjectile(Entity e) {
		
	}
}
