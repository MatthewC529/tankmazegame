package com.treehouseelite.tank.testbed;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.treehouseelite.tank.exception.LogException;
import com.treehouseelite.tank.references.Log;
import com.treehouseelite.tank.references.Reference;
import com.treehouseelite.tank.references.Util;

@SuppressWarnings({ "unused", "serial" })
// screw them warnings, too muy in this GUI... I'll stop now...
public class TEConsole extends JFrame {
	
	private String prevText;
	
	private static Log log;
	
	private int spacing = 5;
	private JPanel tePanel = new JPanel();
	private GridBagConstraints constraints = new GridBagConstraints();
	
	private Font defFont = new Font("Courier New", Font.BOLD, 15);
	private Font disFont = new Font("Courier New", Font.BOLD, 13);
	private Color defFore = Color.WHITE;
	private Color defBack = Color.BLACK;
	
	private JTextField cInput = new JTextField();
	
	private Color matrixColor = Util.convertNormHex("00CC00", 1.0f);
	private Color matrixRed = Util.convertNormHex("DE0D0D", 1.0f);
	private Color matrixBlue = Util.convertNormHex("1B5BF2", 1.0f);
	private final String changeColorCommand = "console.color = ";
	private final String helpCommand = "help";
	private final String changeMode = "mode.debug = ";
	
	public TEConsole() {
		super(Reference.TITLE + " Console");
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		try {
			log = new Log();
		} catch (LogException e) {
		}
		this.add(tePanel);
		tePanel.setLayout(null);
		this.setResizable(false);
		this.setBounds(0, 0, 420, 520);
		
		// Drawing stuff
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel console = new JPanel();
		final JTextPane cOverview = new JTextPane();
		
		Border defBorder = BorderFactory.createLoweredBevelBorder();
		
		tePanel.add(panel1);
		tePanel.add(panel2);
		tePanel.add(console);
		panel1.setLayout(null);
		panel2.setLayout(null);
		console.setLayout(null);
		
		panel1.setLocation(spacing, spacing);
		panel1.setSize(205, 255);
		
		console.setLocation(spacing, 260 + spacing + spacing);
		console.setSize(405, 215);
		
		panel2.setLocation(205 + (2 * spacing), spacing);
		panel2.setSize(195, 255);
		panel1.setBorder(defBorder);
		panel2.setBorder(defBorder);
		console.setBorder(defBorder);
		panel1.setVisible(true);
		panel2.setVisible(true);
		console.setVisible(true);
		/* CONSOLE COMPONENTS */
		
		cOverview.setBackground(defBack);
		cOverview.setForeground(defFore);
		cOverview.setLocation(0, 0);
		cOverview.setSize(405, 186);
		cOverview.setText("Feel free to write a command!");
		cOverview.setFont(disFont);
		cOverview.setEditable(true);
		cInput.setBackground(defBack);
		cInput.setForeground(defFore);
		cInput.setFont(defFont);
		cInput.setLocation(-1, 185);
		cInput.setSize(405, 30);
		cInput.setBorder(BorderFactory.createLineBorder(defFore, 1));
		cInput.setVisible(true);
		cInput.setText(" Enter input here...");
		
		JLabel panel1Title = new JLabel();
		JLabel debugSelect = new JLabel();
		JLabel reloadTitle = new JLabel("Reload Needed");
		
		ButtonGroup reloadGroup = new ButtonGroup();
		final JRadioButton reloadOn = new JRadioButton("On");
		final JRadioButton reloadOff = new JRadioButton("Off");
		reloadGroup.add(reloadOn);
		reloadGroup.add(reloadOff);
		final JRadioButton debugOn = new JRadioButton("Debug");
		final JRadioButton debugOff = new JRadioButton("Normal");
		final JRadioButton ltcOn = new JRadioButton("On");
		final JRadioButton ltcOff = new JRadioButton("Off");
		ButtonGroup ltcGroup = new ButtonGroup();
		ltcGroup.add(ltcOff);
		ltcGroup.add(ltcOn);
		
		JLabel vectorControl = new JLabel("Numerical Controls");
		final JLabel velTitle = new JLabel("Shell Starting Velocity: " + Float.toString(Reference.velCoEfficient * 100));
		final JSlider velSlide = new JSlider();
		final JTextField velInput = new JTextField(Float.toString(Reference.velCoEfficient * 100));
		final JLabel aDragTitle = new JLabel("Air Drag Constant: " + Float.toString(Reference.aeroDragConstant * 1000000));
		final JSlider aDragSlider = new JSlider();
		final JTextField aDragField = new JTextField();
		
		cInput.addMouseListener(new HandlerHandler1());
		cInput.addKeyListener(new KeyListener() {
			
			@Override
			public void keyPressed(KeyEvent e) {
				
				boolean validCommand = false;
				
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					JTextField field = (JTextField) e.getSource();
					String text = field.getText();
					prevText = cOverview.getText();
					
					// Laser Tank!
					if (text.equalsIgnoreCase(" Activate LaserTank")) {
						cInput.setText(" ");
						Reference.laserTankCode = true;
						ltcOn.setSelected(true);
					}
					
					// Reload
					if (text.equalsIgnoreCase(" reload")) {
						validCommand = true;
						cInput.setText(" reload = ");
					} else if (text.equalsIgnoreCase(" reload = ")) {
						String sub = text.substring(text.indexOf("=") + 2);
						boolean bool = Boolean.parseBoolean(sub);
						if (bool) {
							Reference.reloadOn = true;
						} else if (!bool) {
							Reference.reloadOn = false;
						}
						validCommand = true;
						cInput.setText(" ");
					}
					
					// Help Commands
					if (text.equalsIgnoreCase(" help")) {
						String help = " ALL COMMANDS ARE NOT CASE SENSITIVE\n   velCoEfficient to Modify Projectile Velocity\n   airDrag to Modify airDrag\n   debugMode to modify game (true for debug)\n   close to exit this screen\n   exit to exit entire program\n   help to display this text";
						text = help;
						cInput.setText(" ");
						validCommand = true;
					}
					
					// close
					if (text.equalsIgnoreCase(" close")) {
						close();
					}
					
					// exit
					if (text.equalsIgnoreCase(" exit")) {
						try {
							log.finishUp();
						} catch (Exception e1) {
						}
						System.runFinalization();
						System.exit(0);
					}
					// game mode command
					if (text.equalsIgnoreCase(" debugMode")) {
						validCommand = true;
						cInput.setText(" debugMode = ");
					} else if (text.contains(" debugMode = ")) {
						String sub = text.substring(text.lastIndexOf('=') + 2);
						boolean bool = Boolean.parseBoolean(sub);
						if (bool) {
							debugOn.setSelected(true);
							Reference.debug = true;
						} else if (!bool) {
							debugOff.setSelected(true);
							Reference.debug = false;
						}
						validCommand = true;
						cInput.setText(" ");
					}
					
					// changing Velocity CoEfficient for Projectiles
					if (text.equalsIgnoreCase(" velCoEfficient") || text.equalsIgnoreCase(" velocityCoEfficient")) {
						cInput.setText(" Velocity CoEfficient = ");
						validCommand = true;
					} else if (text.contains(" Velocity CoEfficient = ")) {
						String value = text.substring(text.lastIndexOf('=') + 1);
						if (!value.contains("f")) {
							value = value + "f";
						}
						float coefficient = Float.parseFloat(value);
						Reference.velCoEfficient = coefficient;
						velTitle.setText("Shell Starting Velocity:  " + Float.toString(coefficient));
						velInput.setText(Float.toString(coefficient));
						cInput.setText(" ");
						validCommand = true;
					}
					
					if (!validCommand) {
						text = String.format("The Command ('%s ') is invalid!", text);
					}
					
					cOverview.setText(String.format("%s\n%s", text, prevText));
				}
				if ((e.getKeyCode() == KeyEvent.VK_BACK_SPACE) || (e.getKeyCode() == KeyEvent.VK_DELETE)) {
					if (cInput.getCaretPosition() <= 1) {
						cInput.setCaretPosition(2);
					}
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyTyped(KeyEvent e) {
			}
		});
		
		console.add(cOverview);
		console.add(cInput);
		
		/* Flag Control */
		
		panel1Title.setText("Non-Numerical Controls");
		panel1Title.setLocation(10, 10);
		panel1Title.setSize(160, 20);
		panel1Title.setFont(new Font("Times New Roman", Font.BOLD, 14));
		panel1Title.setBorder(BorderFactory.createEtchedBorder());
		
		debugSelect.setText("Game Mode");
		debugSelect.setLocation(10, 40);
		debugSelect.setSize(70, 20);
		debugSelect.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		
		ButtonGroup debugSelection = new ButtonGroup();
		debugSelection.add(debugOn);
		debugSelection.add(debugOff);
		if (Reference.debug) {
			debugOn.setSelected(true);
		}
		if (!Reference.debug) {
			debugOff.setSelected(true);
		}
		
		debugOn.setLocation(10, 60);
		debugOn.setSize(60, 20);
		debugOff.setLocation(10, 80);
		debugOff.setSize(65, 20);
		
		debugOn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent evt) {
				
				JRadioButton button = (JRadioButton) evt.getSource();
				if (button.isSelected()) {
					Reference.debug = true;
				}
			}
			
		});
		debugOff.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				JRadioButton but = (JRadioButton) evt.getSource();
				if (but.isSelected()) {
					Reference.debug = false;
				}
			}
		});
		
		reloadTitle.setLocation(100, 40);
		reloadTitle.setSize(90, 20);
		reloadTitle.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		
		Point p = reloadTitle.getLocation();
		reloadOn.setLocation(p.x, p.y + 20);
		reloadOn.setSize(40, 20);
		reloadOn.setSelected(true);
		
		reloadOff.setLocation(p.x, p.y + 40);
		reloadOff.setSize(40, 20);
		
		if (Reference.reloadOn) {
			reloadOn.setSelected(true);
		}
		if (!Reference.reloadOn) {
			reloadOff.setSelected(true);
		}
		
		reloadOn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				JRadioButton button = (JRadioButton) evt.getSource();
				if (button.isSelected()) {
					Reference.reloadOn = true;
				}
			}
		});
		reloadOff.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				JRadioButton button = (JRadioButton) evt.getSource();
				if (button.isSelected()) {
					Reference.reloadOn = false;
				}
			}
		});
		
		JLabel ltcLabel = new JLabel("LAZOR TANK");
		
		ltcLabel.setLocation(10, 120);
		ltcLabel.setSize(80, 20);
		
		ltcOn.setLocation(10, 140);
		ltcOn.setSize(50, 20);
		ltcOff.setLocation(10, 160);
		ltcOff.setSize(50, 20);
		ltcOff.setSelected(true);
		
		ltcOn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				
				JRadioButton button = (JRadioButton) evt.getSource();
				
				if (button.isSelected()) {
					Reference.laserTankCode = true;
				}
				
			}
		});
		
		ltcOff.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				
				JRadioButton button = (JRadioButton) evt.getSource();
				
				if (button.isSelected()) {
					Reference.laserTankCode = false;
				}
				
			}
		});
		
		panel1.add(ltcLabel);
		panel1.add(panel1Title);
		panel1.add(debugSelect);
		panel1.add(debugOn);
		panel1.add(debugOff);
		panel1.add(reloadTitle);
		panel1.add(reloadOn);
		panel1.add(reloadOff);
		panel1.add(ltcOn);
		panel1.add(ltcOff);
		
		/* Numerical Modifiers */
		
		vectorControl.setLocation(10, 10);
		vectorControl.setSize(125, 20);
		vectorControl.setFont(new Font("Times New Roman", Font.BOLD, 14));
		vectorControl.setBorder(BorderFactory.createEtchedBorder());
		
		velTitle.setLocation(10, 40);
		velTitle.setSize(160, 20);
		int def = 15;
		velSlide.setLocation(10, 60);
		velSlide.setSize(160, 20);
		velSlide.setMinimum(10);
		velSlide.setMaximum(500);
		velSlide.setValue(def);
		velSlide.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider event = (JSlider) e.getSource();
				String text = Integer.toString(event.getValue()) + "f";
				float velocity = event.getValue() / 100;
				velTitle.setText("Shell Starting Velocity: " + Float.toString(event.getValue()));
				velInput.setText(Float.toString(event.getValue()));
				Reference.velCoEfficient = velocity;
			}
		});
		
		velInput.setLocation(15, 80);
		velInput.setSize(60, 20);
		velInput.addKeyListener(new KeyListener() {
			
			@Override
			public void keyPressed(KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
					JTextField field = (JTextField) evt.getSource();
					String text = field.getText() + "f";
					float velocity = Float.parseFloat(text);
					velTitle.setText("Shell Starting Velocity: " + Float.toString(velocity));
					Reference.velCoEfficient = velocity;
					velSlide.setValue((int) velocity);
					velInput.setText(Float.toString(velocity));
				}
				
			}
			
			@Override
			public void keyReleased(KeyEvent evt) {
				
			}
			
			@Override
			public void keyTyped(KeyEvent evt) {
				
			}
			
		});
		
		int minAD = 15;
		int maxAD = 1000;
		
		aDragTitle.setLocation(10, 110);
		aDragTitle.setSize(150, 20);
		
		aDragSlider.setLocation(10, 135);
		aDragSlider.setSize(150, 20);
		float interpretedADrag = Reference.aeroDragConstant * 1000000;
		aDragSlider.setValue((int) interpretedADrag);
		aDragSlider.setMinimum(1);
		aDragSlider.setMaximum(maxAD);
		aDragSlider.setMinorTickSpacing(1);
		aDragSlider.setMajorTickSpacing(10);
		aDragSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider slide = (JSlider) e.getSource();
				float airdrag = (float) slide.getValue() / 1000000;
				Reference.aeroDragConstant = airdrag;
				aDragField.setText(Integer.toString(slide.getValue()));
				aDragTitle.setText("Air Drag Constant: " + Integer.toString(slide.getValue()));
			}
		});
		
		aDragField.setLocation(20, 160);
		aDragField.setSize(50, 20);
		aDragField.setText(Float.toString(Reference.aeroDragConstant * 1000000));
		aDragField.addKeyListener(new KeyListener() {
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					
					JTextField field = (JTextField) arg0.getSource();
					String text = field.getText() + "f";
					float airdrag = Float.parseFloat(text) / 1000000;
					Reference.aeroDragConstant = airdrag;
					aDragField.setText(Float.toString(airdrag));
					aDragTitle.setText("Air Drag Constant: " + Float.toString(airdrag));
					aDragSlider.setValue((int) (airdrag * 1000000));
					
				}
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				
			}
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				
			}
			
		});
		
		panel2.add(vectorControl);
		panel2.add(velTitle);
		panel2.add(velSlide);
		panel2.add(velInput);
		panel2.add(aDragTitle);
		panel2.add(aDragSlider);
		panel2.add(aDragField);
		panel2.setToolTipText(String.format("Use Text Fields for precise values,\n Sliders only allow round integers!"));
		
		tePanel.setPreferredSize(tePanel.getSize());
		
		this.setVisible(true);
	}
	
	private void close() {
		this.dispose();
	}
	
	private class HandlerHandler1 extends MouseAdapter implements MouseListener {
		
		@Override
		public void mouseClicked(MouseEvent evt) {
			cInput.setText(" ");
		}
		
	}
	
}
