package com.treehouseelite.tank.game;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Point;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.treehouseelite.tank.exception.AssetException;
import com.treehouseelite.tank.game.entity.Enemy;
import com.treehouseelite.tank.game.entity.Entity;
import com.treehouseelite.tank.game.entity.Player;
import com.treehouseelite.tank.game.entity.Projectile;
import com.treehouseelite.tank.game.entity.ai.BlueAI;
import com.treehouseelite.tank.game.entity.ai.EntityAI;
import com.treehouseelite.tank.game.entity.ai.OrangeAI;
import com.treehouseelite.tank.game.entity.ai.PinkAI;
import com.treehouseelite.tank.game.entity.ai.RedAI;
import com.treehouseelite.tank.game.handlers.AssetHandler;
import com.treehouseelite.tank.game.handlers.InputHandler;
import com.treehouseelite.tank.game.handlers.OutputHandler;
import com.treehouseelite.tank.game.handlers.TileHandler;
import com.treehouseelite.tank.references.Reference;
import com.treehouseelite.tank.references.Util;

//import java.awt.Rectangle;

/**
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 *         Michael Lucido (Best Friend, Treehouse Elite, Tester) Nathyn Vega
 *         (Alpha Tester, Supporter) Kevin MacLeod (Royalty Free Music)
 * 
 *         Licensed Under the GNU LGPL v3 in Concurrence with the GPL v3
 * 
 *         This file is part of Tank Maze Game.
 * 
 *         Tank Maze Game is free software: you can redistribute it and/or
 *         modify it under the terms of the GNU Lesser General Public License in
 *         concurrence with the GNU General Public License as published by the
 *         Free Software Foundation, either version 3 of the License, or any
 *         later version.
 * 
 *         Tank Maze Game is distributed in the hope that it will be
 *         entertaining, but WITHOUT ANY WARRANTY; without even the implied
 *         warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 *         the GNU Lesser General Public License and GNU General Public License
 *         for more details.
 * 
 *         You should have received a copy of the GNU General Public License and
 *         GNU Lesser General Public License along with Tank Maze Game. If not,
 *         see <http://www.gnu.org/licenses/>
 **/

public class Level extends BasicGameState {
	
	public static AssetHandler asset = new AssetHandler();
	static OutputHandler out = new OutputHandler();
	
	public static GameContainer container;
	public StateBasedGame game;
	
	public static float MouseX = 0;
	public static float MouseY = 0;
	public float RectX = 0;
	public float RectY = 0;
	public int tileAmount = 0;
	public static int mapID = 1;
	public static int delta;
	public static int score = 0;
	private static int EntityAmount = 0;
	private static int ActiveEntityAmount = 0;
	
	private int FPS = 0;
	
	public static Image mapImage;
	public static TileHandler Tile = new TileHandler();
	public Point mousePoint;
	public Circle mouseCirc;
	
	public static boolean actuallyCalled = false;
	public static boolean drawConsole = false;
	
	public static Player p;
	public static Enemy Blinky, Pinky, Inky, Clyde;
	public static EntityAI AGGRESSIVE, AMBUSH, HIT_RUN, SORTOFRANDOM;
	public InputHandler IHandler;
	
	public Level(int id) {
		
	}
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		
		Reference.initializeTileLayer();
		
		try {
			asset.initAssets();
			OutputHandler.initFont();
			
		} catch (AssetException e) {
			e.printStackTrace();
		}
		
		container.setUpdateOnlyWhenVisible(true);
		container.setShowFPS(false);
		container.setSmoothDeltas(false);
		container.setVerbose(true);
		
		Level.container = container;
		this.game = game;
		
		if (actuallyCalled) {
			TileHandler.initTileMap();
			AGGRESSIVE = new RedAI();
			AMBUSH = new PinkAI();
			HIT_RUN = new BlueAI();
			SORTOFRANDOM = new OrangeAI();
			
			p = new Player("Player1");
			System.out.println("Player Spawned!");
			Blinky = new Enemy("Shadow", AGGRESSIVE);
			Pinky = new Enemy("Speedy", AMBUSH);
			Inky = new Enemy("Bashful", HIT_RUN);
			Clyde = new Enemy("Pokey", SORTOFRANDOM);
			System.out.println("Ghosts Spawned!");
			EntityAI.spawned = true;
			
		} else {
			;
		}
		System.out.println("LEVEL LOADED!");
	}
	
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		
		Tile.drawTileMap(Reference.tiledLayer, TileHandler.tMapTiles, g);
		if (Reference.debug) {
			displayTileBounds(Reference.tiledLayer, g);
		}
		drawEntities();
		drawStrings(g);
	}
	
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		Input in = container.getInput();
		
		MouseX = in.getMouseX();
		MouseY = in.getMouseY();
		RectX = Reference.Map32.getX();
		RectY = Reference.Map32.getY();
		EntityAmount = Entity.entityList.size();
		ActiveEntityAmount = Enemy.enemyList.size() + Projectile.activeProjectiles.size() + 1;
		Level.delta = delta;
		Reference.defProjectileVelocity = .13f * Level.delta;
		
		IHandler = new InputHandler(in, InputHandler.KEYBOARD);
		IHandler = new InputHandler(in, InputHandler.MOUSE);
		
		if (in.isKeyDown(Input.KEY_SPACE) && Reference.laserTankCode) {
			p.fire();
		}
		
		p.update(in);
		updateNonPlayerEntities();
		
		FPS = container.getFPS();
	}
	
	@Override
	public int getID() {
		
		return 2;
	}
	
	/** @deprecated **/
	@Deprecated
	protected void drawMap(Graphics g) {
		
		g.drawImage(mapImage, Reference.MAP_X, Reference.MAP_Y);
		
	}
	
	protected void drawStrings(Graphics g) {
		
		if (Reference.debug) {
			OutputHandler.write("FPS: " + Integer.toString(FPS), 11, 10);
			OutputHandler.write(String.format("Mouse X: %s, Mouse Y: %s", MouseX, MouseY), 11, 30);
			OutputHandler.write(String.format("Rect X: %s, Y: %s", RectX, RectY), 11, 50);
			OutputHandler.write("Amount of Tiles: " + (Reference.tiledLayer.length * Reference.tiledLayer[0].length), 11, 70);
			OutputHandler.write(String.format("Amount of Entities = %s", Integer.toString(EntityAmount)), 11, 90);
			OutputHandler.write(String.format("Active Entities = %s", Integer.toString(ActiveEntityAmount)), 11, 110);
			OutputHandler.write("Currently Loaded: " + p.isReloaded(), 11, 130);
			OutputHandler.write("Current Projectile/Case: " + Entity.projectileNum, 11, 150);
			OutputHandler.write("Curent Air Drag Constant: " + Reference.aeroDragConstant, 11, 170);
			OutputHandler.write("Current Velocity CoEfficient: " + Reference.velCoEfficient, 11, 190);
			
			OutputHandler.write("Amount of Entities is Accumulative", 11, 666);
		} else {
			String curTime = Util.getTime();
			String scoreStr = Util.convertScore(score);
			OutputHandler.write("Time: " + curTime, 11, 10);
			OutputHandler.write("Score: " + scoreStr, 550, 10);
		}
	}
	
	protected void displayTileBounds(Rectangle[][] tileLayer, Graphics g) {
		g.setColor(Color.white);
		for (int x = 0; x < tileLayer.length; x++) {
			for (int y = 0; y < tileLayer[0].length; y++) {
				g.fill(tileLayer[x][y]);
			}
		}
		g.setColor(Color.magenta);
		
		for (int s = 0; s < TileHandler.collisionTiles.size(); s++) {
			Rectangle r = TileHandler.collisionTiles.get(s);
			g.fill(r);
		}
		g.setColor(Color.orange);
		g.fill(p.boundingBox);
		
		for (int z = 0; z < Entity.teleportingTiles.length; z++) {
			Rectangle r = Entity.teleportingTiles[z];
			g.fill(r);
		}
	}
	
	protected void drawEntities() {
		ArrayList<Entity> list = Entity.entityList;
		
		for (int i = 0; i < list.size(); i++) {
			
			list.get(i).drawEntity(list.get(i));
			
		}
	}
	
	protected void updateNonPlayerEntities() {
		ArrayList<Enemy> list = Enemy.enemyList;
		
		for (int i = 0; i < list.size(); i++) {
			list.get(i).update();
		}
		
		ArrayList<Projectile> pList = Projectile.activeProjectiles;
		
		for (int p = 0; p < pList.size(); p++) {
			pList.get(p).update();
		}
	}
	
}
