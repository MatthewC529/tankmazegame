package com.treehouseelite.tank.game.entity;

import java.util.Random;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

import com.treehouseelite.tank.game.Level;
import com.treehouseelite.tank.game.handlers.AssetHandler;
import com.treehouseelite.tank.game.handlers.TileHandler;
import com.treehouseelite.tank.references.Reference;

public class Player extends Entity {
	
	String id;
	static Animation moveLeft;
	static Animation moveRight;
	static Animation moveUp;
	static Animation moveDown;
	public Rectangle curRect;
	Rectangle originalBoundingBox;
	Shape rotatedBoundingBox;
	
	private static AssetHandler assets = new AssetHandler();
	
	Projectile p1;
	Projectile p2;
	Projectile p3;
	Projectile p4;
	Projectile p5;
	
	Rectangle[][] tileLayer = Reference.tiledLayer;
	Rectangle[] autoOccupiedTiles = { tileLayer[12][9], tileLayer[12][10], tileLayer[11][10], tileLayer[13][10], tileLayer[12][8], tileLayer[12][8], tileLayer[12][7], tileLayer[12][6], tileLayer[11][6], tileLayer[10][6], tileLayer[9][6], tileLayer[13][6], tileLayer[14][6], tileLayer[15][6] };
	
	float prevXVal = 0;
	float prevYVal = 0;
	float initialX;
	float initialY;
	
	public Player(String id) {
		this.id = id;
		Entity.entityList.add(this);
		initAnims();
		
		randomPlayerPlacement();
		this.boundingBox = getBoundingBox(this.curRect);
		this.originalBoundingBox = boundingBox;
		this.rotatedBoundingBox = this.boundingBox.transform(Transform.createRotateTransform(90, this.boundingBox.getCenterX(), y));
		
	}
	
	/**
	 * Updates the entity for collision detection and movement
	 * 
	 * @param i
	 *            Somekind of Slick2D input
	 * @author Matthew Crocco (Treehouse Elite)
	 */
	public void update(Input i) {
		
		int delta = Level.delta;
		float x = this.boundingBox.getX();
		float y = this.boundingBox.getY();
		
		// Getting Delta values for Stopping or continuing animation
		float deltaX = x - prevXVal;
		float deltaY = y - prevYVal;
		prevXVal = x;
		prevYVal = y;
		
		float width = this.boundingBox.getWidth();
		float height = this.boundingBox.getHeight();
		this.boundingBox.getHeight();
		this.boundingBox.getWidth();
		
		boolean upCollide = false;
		boolean downCollide = false;
		boolean leftCollide = false;
		boolean rightCollide = false;
		
		if (i.isKeyDown(Input.KEY_W) || (i.isKeyDown(Input.KEY_UP))) {
			this.boundingBox.setBounds(x, y - (0.112f * delta), width, height);
			if (this.isColliding()) {
				upCollide = true;
			}
			this.curAnimation = moveUp;
			this.curDirection = "North";
		}
		
		if (i.isKeyDown(Input.KEY_S) || (i.isKeyDown(Input.KEY_DOWN))) {
			this.boundingBox.setBounds(x, y + (0.112f * delta), width, height);
			if (this.isColliding()) {
				downCollide = true;
			}
			this.curAnimation = moveDown;
			this.curDirection = "South";
		}
		
		if (i.isKeyDown(Input.KEY_D) || (i.isKeyDown(Input.KEY_RIGHT) && !i.isKeyDown(Input.KEY_A))) {
			this.boundingBox.setBounds(this.boundingBox.getX() + (0.112f * delta), y, this.boundingBox.getWidth(), this.boundingBox.getHeight());
			if (this.isColliding()) {
				rightCollide = true;
			}
			this.curAnimation = moveRight;
			this.curDirection = "East";
		}
		
		if (i.isKeyDown(Input.KEY_A) || (i.isKeyDown(Input.KEY_LEFT) && !i.isKeyDown(Input.KEY_D))) {
			
			this.boundingBox.setBounds(this.boundingBox.getX() - (0.112f * delta), y, this.boundingBox.getWidth(), this.boundingBox.getHeight());
			if (this.isColliding()) {
				leftCollide = true;
			}
			this.curAnimation = moveLeft;
			this.curDirection = "West";
		}
		
		if ((i.isKeyPressed(Input.KEY_SPACE) && reloaded) || (i.isKeyPressed(Input.KEY_SPACE) && !Reference.reloadOn)) {
			
			if (!Reference.laserTankCode) {
				@SuppressWarnings("unused")
				Projectile p = new Projectile(this.boundingBox, this.curDirection);
				projectileNum++;
				if (Reference.reloadOn) {
					reloaded = false;
				}
				
			}
			/**
			 * else if(i.isKeyDown(Input.KEY_SPACE) && Reference.laserTankCode){
			 * 
			 * @SuppressWarnings("unused") Projectile proj = new
			 *                             Projectile(this.boundingBox,
			 *                             this.curDirection); projectileNum++;
			 *                             }
			 */
			
		} else {
			reload();
		}
		
		if ((deltaX == 0) && (deltaY == 0)) {
			this.curAnimation.stop();
		} else if ((deltaY != 0) || ((deltaX != 0) && this.curAnimation.isStopped())) {
			this.curAnimation.start();
		}
		
		if (leftCollide || rightCollide || downCollide || upCollide) {
			
			/*
			 * if (leftCollide) { deltaX = deltaX + (0.2f * delta); } if
			 * (rightCollide) { deltaX = deltaX - (0.2f * delta); } if
			 * (upCollide) { deltaY = deltaY + (0.2f * delta); } if
			 * (downCollide) { deltaY = deltaY - (0.2f * delta); }
			 */
			
			this.boundingBox.setLocation(x, y);
		}
		
		if (this.curAnimation.isStopped()) {
			tankMove.stop();
		} else if (!this.curAnimation.isStopped() && !tankMove.playing()) {
			tankMove.loop(1.0f, 0.1f);
		}
		
		if (this.isTeleporting()) {
			if (this.curAnimation.isStopped() && teleportedFrom.equalsIgnoreCase("Left")) {
				// this.curAnimation = enteringFromRight;
				this.boundingBox.setLocation(teleportingTiles[2].getX() - 31, teleportingTiles[2].getY() + 1);
			} else if (this.curAnimation.isStopped() && teleportedFrom.equalsIgnoreCase("Right")) {
				// this.curAnimation = enteringFromLeft;
				this.boundingBox.setLocation(teleportingTiles[0].getX() + 31, teleportingTiles[0].getY() + 1);
			}
		}
		
		if (Reference.debug) {
			if (!reloaded) {
				if (i.isKeyPressed(Input.KEY_R)) {
					reloaded = true;
				}
			}
		}
		
		checkCurTile();
	}
	
	/**
	 * Randomly assigns the entity a tile for spawning
	 * 
	 * @author Matthew Crocco (Treehouse Elite)
	 */
	private void randomPlayerPlacement() {
		
		boolean matching = true;
		Random r = new Random();
		Rectangle selectedRect = null;
		
		for (int i = 0; i < TileHandler.collisionTiles.size(); i++) {
			Rectangle rect = TileHandler.collisionTiles.get(i);
			
			if (matching) {
				this.x = r.nextInt(25);
				this.y = r.nextInt(20);
				selectedRect = Reference.tiledLayer[x][y];
				matching = false;
				i = 0;
			}
			
			if ((rect.getX() == selectedRect.getX()) && (rect.getY() == selectedRect.getY())) {
				matching = true;
			} else {
				this.x = (int) selectedRect.getX();
				this.y = (int) selectedRect.getY();
				this.curRect = selectedRect;
			}
			
			for (int x = 0; x < autoOccupiedTiles.length; x++) {
				if (selectedRect.equals(autoOccupiedTiles[x])) {
					matching = true;
					break;
				}
			}
			
		}
		this.curRect = selectedRect;
		this.initialX = selectedRect.getX();
		this.initialY = selectedRect.getY();
		this.curAnimation = moveLeft;
		this.curDirection = "West";
		System.out.println("sRect X: " + x + " ,sRectY: " + y);
	}
	
	protected void initAnims() {
		moveUp = AssetHandler.tankToNorth;
		moveDown = AssetHandler.tankToSouth;
		moveLeft = AssetHandler.tankToWest;
		moveRight = AssetHandler.tankToEast;
		tankMove = AssetHandler.tankMove;
	}
	
	public void fire() {
		@SuppressWarnings("unused")
		Projectile proj = new Projectile(this.boundingBox, this.curDirection);
		projectileNum++;
	}
	
	public Rectangle getCurRect() {
		return curRect;
	}
}
