package com.treehouseelite.tank.game.entity.ai;

import org.newdawn.slick.geom.Rectangle;

import com.treehouseelite.tank.game.Level;
import com.treehouseelite.tank.game.entity.Enemy;
import com.treehouseelite.tank.game.entity.Player;

public class EntityAI {
	
	Enemy entity;
	Rectangle thisRect;
	
	int scannerRadius = 5;
	
	int targetRectArrayX;
	int targetRectArrayY;
	
	boolean canMoveLeft = true;
	boolean canMoveRight = true;
	boolean canMoveUp = true;
	boolean canMoveDown = true;
	
	public static boolean spawned = false;
	
	public static String ANGRY = "AGGRESSIVE";
	public static String AMBUSH = "AMBUSH";
	public static String RANDOM = "RANDOM";
	public static String GUERILLA = "HIT & RUN";
	
	protected static Player target;
	protected static Rectangle targetRect;
	
	protected boolean firstStepsNeeded = true;
	
	public void launchAI(String Type) {
		
	}
	
	protected void initAStar() {
		target = Level.p;
		targetRect = target.getCurRect();
		
	}
	
}
