package com.treehouseelite.tank;

import java.io.IOException;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.treehouseelite.tank.game.Level;
import com.treehouseelite.tank.game.screens.CinematicsScreen;
import com.treehouseelite.tank.game.screens.RoundResults;
import com.treehouseelite.tank.menus.MainMenu;
import com.treehouseelite.tank.menus.SettingsAndOptions;
import com.treehouseelite.tank.references.Log;
import com.treehouseelite.tank.references.Reference;

public class TankMaze extends StateBasedGame {
	
	private static String title = Reference.TITLE;
	
	/* States */
	private static int main = Reference.MAIN_MENU;
	private static int options = Reference.OPTIONS;
	private static int play = Reference.PLAY;
	private static int cinematics = Reference.CINEMATICS;
	private static int roundresult = Reference.ROUND_RESULTS;
	
	public static Log log;
	
	public TankMaze(String name) {
		super(name);
		this.addState(new MainMenu(main));
		this.addState(new SettingsAndOptions(options));
		this.addState(new Level(play));
		this.addState(new RoundResults(roundresult));
		this.addState(new CinematicsScreen(cinematics));
	}
	
	public static void main(String[] args) {
		
		AppGameContainer agc;
		try {
			agc = new AppGameContainer(new TankMaze(title));
			agc.setIcons(Reference.gameIcons);
			agc.setDisplayMode(Reference.GUI_WIDTH, Reference.GUI_HEIGHT, false);
			agc.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		
		try {
			log = new Log(Long.MAX_VALUE);
		} catch (IOException e) {
			
		}
		
		this.getState(main);
		this.getState(options);
		this.getState(play).init(gc, this);
		this.getState(roundresult);
		this.getState(cinematics);
		this.enterState(main);
	}
}
