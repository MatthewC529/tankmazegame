package com.treehouseelite.tank.game.screens;

/** 
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 * @Lib LightWeight Java Gaming Library and Slick2D Library
 * 
 * @Thanks Michael Lucido (Best Friend, Alpha Tester, Treehouse Elite)
 * @Thanks Kevin MacLeod (Royalty Free Music)
 * 
 * @License GNU LGPL v3 in Concurrence with GNU GPL v3
 * @Copyright GNU LGPL v3 in Concurrence with GNU GPL v3 
 * 
 * @Warranty 
 *     This file is part of Foobar.
 *
 *  Tank Maze Game is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License in concurrence with the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  Tank Maze Game is distributed in the hope that it will be entertaining,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License and GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License and
 *  GNU Lesser General Public License along with Tank Maze Game.  If not, see <http://www.gnu.org/licenses/>
 **/

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class CinematicsScreen extends BasicGameState {
	
	public CinematicsScreen(int id) {
		
	}
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		
	}
	
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		
	}
	
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		
	}
	
	@Override
	public int getID() {
		
		return 4;
	}
	
}
