package com.treehouseelite.tank.game.entity;

import java.util.ArrayList;

import org.newdawn.slick.Animation;

import com.treehouseelite.tank.game.entity.ai.EntityAI;
import com.treehouseelite.tank.game.entity.ai.RedAI;
import com.treehouseelite.tank.game.handlers.AssetHandler;
import com.treehouseelite.tank.references.Reference;

public class Enemy extends Entity {
	
	String id;
	EntityAI AI;
	
	public boolean upCollide = false;
	public boolean downCollide = false;
	public boolean leftCollide = false;
	public boolean rightCollide = false;
	
	public static ArrayList<Enemy> enemyList = new ArrayList<Enemy>();
	
	Animation bN, rN, pN, oN, bS, rS, pS, oS, bE, rE, pE, oE, bW, rW, pW, oW;
	
	public Enemy(String id, EntityAI AI) {
		this.id = id;
		this.AI = AI;
		
		Entity.entityList.add(this);
		enemyList.add(this);
		initAnims();
		
		enemyPlacement(this.id);
		this.boundingBox = getBoundingBox(this.curRect);
		this.checkCurTile();
		
		System.out.println("Loading AI...");
		this.launchAI();
		
	}
	
	private void enemyPlacement(String id) {
		
		if ((id == "Shadow") || (id == "Blinky")) {
			this.curRect = Reference.tiledLayer[12][9];
			this.x = (int) curRect.getX();
			this.y = (int) curRect.getY();
			this.curAnimation = rN;
			
		} else if (id.equalsIgnoreCase("Speedy") || id.equalsIgnoreCase("Pinky")) {
			this.curRect = Reference.tiledLayer[12][10];
			this.x = (int) curRect.getX();
			this.y = (int) curRect.getY();
			this.curAnimation = pN;
			
		} else if (id.equalsIgnoreCase("Bashful") || id.equalsIgnoreCase("Inky")) {
			this.curRect = Reference.tiledLayer[11][10];
			this.x = (int) curRect.getX();
			this.y = (int) curRect.getY();
			this.curAnimation = bW;
			
		} else if (id.equalsIgnoreCase("Pokey") || id.equalsIgnoreCase("Clyde")) {
			this.curRect = Reference.tiledLayer[13][10];
			this.x = (int) curRect.getX();
			this.y = (int) curRect.getY();
			this.curAnimation = oE;
		}
	}
	
	public void update() {
		upCollide = false;
		downCollide = false;
		leftCollide = false;
		rightCollide = false;
		
		this.updateAI();
		this.checkCurTile();
		
	}
	
	protected void initAnims() {
		bN = AssetHandler.blueToNorth;
		rN = AssetHandler.redToNorth;
		pN = AssetHandler.pinkToNorth;
		oN = AssetHandler.orangeToNorth;
		
		bS = AssetHandler.blueToSouth;
		rS = AssetHandler.redToSouth;
		pS = AssetHandler.pinkToSouth;
		oS = AssetHandler.orangeToSouth;
		
		bE = AssetHandler.blueToEast;
		rE = AssetHandler.redToEast;
		pE = AssetHandler.pinkToEast;
		oE = AssetHandler.orangeToEast;
		
		bW = AssetHandler.blueToWest;
		rW = AssetHandler.redToWest;
		pW = AssetHandler.pinkToWest;
		oW = AssetHandler.orangeToWest;
	}
	
	private void launchAI() {
		
		if (this.id == "Shadow") {
			((RedAI) AI).start(this);
		}
		
	}
	
	private void updateAI() {
		if (this.id == "Shadow") {
			((RedAI) AI).startAStar();
		}
	}
	
}
